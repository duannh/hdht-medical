package edu.poly.medical.hdht.service.mapper;

import edu.poly.medical.hdht.domain.Blog;
import edu.poly.medical.hdht.service.dto.BlogDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class BlogMapper implements EntityMapper<Blog, BlogDTO> {

    @Override
    public Blog toEntity(BlogDTO dto) {
        Blog entity = new Blog();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setText(dto.getText());
        entity.setImage(dto.getImage());
        entity.setCreateDate(dto.getCreateDate());
        return entity;
    }

    @Override
    public BlogDTO toDto(Blog entity) {
        BlogDTO dto = new BlogDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setText(entity.getText());
        dto.setImage(entity.getImage());
        dto.setCreateDate(entity.getCreateDate());
        return dto;
    }

    @Override
    public List<BlogDTO> toDTOs(List<Blog> entities) {
        return entities.stream()
                .filter(Objects::isNull)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Blog> toEntities(List<BlogDTO> dtos) {
        return dtos.stream()
                .filter(Objects::isNull)
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

    @Override
    public Blog toEntityFromId(Long id) {
        if (id == null) {
            return null;
        }
        Blog blog = new Blog();
        blog.setId(id);
        return blog;
    }
}
