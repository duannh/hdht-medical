package edu.poly.medical.hdht.web.rest;

import edu.poly.medical.hdht.service.MedicalHistoryService;
import edu.poly.medical.hdht.service.dto.MedicalHistoryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static edu.poly.medical.hdht.common.ResourcesConstants.*;

/**
 * The type Medical history resources.
 */
@RestController
@RequestMapping(RESOURCE_API)
public class MedicalHistoryResources {

    @Autowired
    private MedicalHistoryService medicalHistoryService;

    /**
     * Gets all medical histories.
     *
     * @return the all medical histories
     */
    @GetMapping(MEDICAL_HISTORY_MAPPING)
    public ResponseEntity<List<MedicalHistoryDTO>> getAllMedicalHistories(){
            List<MedicalHistoryDTO> medicalHistory = medicalHistoryService.findAll();
        return ResponseEntity.ok(medicalHistory);
    }

    /**
     * Create medical history response entity.
     *
     * @param dto the dto
     * @return the response entity
     */
    @PostMapping(MEDICAL_HISTORY_MAPPING)
    public ResponseEntity<MedicalHistoryDTO> createMedicalHistory(@Valid @RequestBody MedicalHistoryDTO dto) {
        if (dto.getId() != null) {
            return ResponseEntity.badRequest().build();
        }
        return Optional.ofNullable(medicalHistoryService.save(dto))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Update medical history response entity.
     *
     * @param dto the dto
     * @return the response entity
     */
    @PutMapping(MEDICAL_HISTORY_MAPPING)
    public ResponseEntity<MedicalHistoryDTO> updateMedicalHistory(@Valid @RequestBody MedicalHistoryDTO dto) {
        if (dto.getId() == null) {
            return ResponseEntity.badRequest().build();
        }
        return Optional.ofNullable(medicalHistoryService.save(dto))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Gets medical history by id.
     *
     * @param id the id
     * @return the medical history by id
     */
    @GetMapping(MEDICAL_HISTORY_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<MedicalHistoryDTO> getMedicalHistoryById(@PathVariable Long id) {
        return medicalHistoryService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Delete medical history by id response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping(MEDICAL_HISTORY_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<Void> deleteMedicalHistoryById(@PathVariable Long id) {
        medicalHistoryService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}