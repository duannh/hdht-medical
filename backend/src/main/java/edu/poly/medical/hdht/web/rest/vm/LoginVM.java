package edu.poly.medical.hdht.web.rest.vm;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * The type Login vm.
 */
@Data
public class LoginVM {

    @NotNull
    @Size(min = 1, max = 50)
    private String phone;

    private String password;

    private Boolean rememberMe;

}
