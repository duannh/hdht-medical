package edu.poly.medical.hdht.service.impl;

import edu.poly.medical.hdht.domain.Blog;
import edu.poly.medical.hdht.repository.BlogRepository;
import edu.poly.medical.hdht.service.BlogService;
import edu.poly.medical.hdht.service.dto.BlogDTO;
import edu.poly.medical.hdht.service.mapper.BlogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BlogServiceImpl implements BlogService {
    @Autowired
    private BlogRepository blogRepository;
    @Autowired
    private BlogMapper blogMapper;

    @Override
    public List<BlogDTO> findAll() {
        return blogRepository.findAll().stream()
                .map(blogMapper::toDto)
                .collect(Collectors.toList());
}

    @Override
    public BlogDTO save(BlogDTO dto) {
        Blog entity = blogMapper.toEntity(dto);
        return Optional.ofNullable(blogRepository.save(entity)).map(blogMapper::toDto).orElse(null);
    }

    @Override
    public Optional<BlogDTO> findById(Long aLong) {
        return blogRepository.findById(aLong).map(blogMapper::toDto);
    }

    @Override
    public void deleteById(Long aLong) {
        blogRepository.deleteById(aLong);
    }

    @Override
    public BlogDTO findBlogById(Long id) {
        return blogRepository.findBlogById(id);
    }


}
