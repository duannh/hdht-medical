package edu.poly.medical.hdht.service.impl;

import edu.poly.medical.hdht.domain.*;
import edu.poly.medical.hdht.repository.AppointmentRepository;
import edu.poly.medical.hdht.service.AppointmentService;
import edu.poly.medical.hdht.service.dto.*;
import edu.poly.medical.hdht.service.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static edu.poly.medical.hdht.common.Constants.APPOINTMENT_REQUEST;

/**
 * The type Appointment service.
 */
@Service
public class AppointmentServiceImpl implements AppointmentService {

    @Autowired
    private AppointmentMapper appointmentMapper;

    @Autowired
    private NurseMapper nurseMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private DoctorMapper doctorMapper;

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private DepartmentMapper departmentMapper;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Override
    public AppointmentDTO save(AppointmentDTO dto) {
        Appointment appointment = appointmentMapper.toEntity(dto);

        return Optional.ofNullable(appointmentRepository.save(appointment))
                .map(appointmentMapper::toDto)
                .orElse(null);
    }

    @Override
    public List<AppointmentDTO> findAll() {
        return appointmentRepository.findAll()
                .stream().map(appointmentMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<AppointmentDTO> findAllByStatusIsRequest() {
        return appointmentRepository.findAllByStatus(APPOINTMENT_REQUEST)
                .stream()
                .map(appointmentMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<AppointmentDTO> findAllByStatus(Integer status) {
        return appointmentRepository.findAllByStatus(status)
                .stream()
                .map(appointmentMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<AppointmentDTO> findAllByCreatedBy(UserDTO userDTO) {
        User user = userMapper.toEntity(userDTO);

        return appointmentRepository.findAllByCreatedBy(user)
                .stream()
                .map(appointmentMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<AppointmentDTO> findAllByDoctor(DoctorDTO doctorDTO) {
        Doctor doctor = doctorMapper.toEntity(doctorDTO);

        return appointmentRepository.findAllByDoctor(doctor)
                .stream()
                .map(appointmentMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<AppointmentDTO> findAllByPatient(PatientDTO patientDTO) {
        Patient patient = patientMapper.toEntity(patientDTO);

        return appointmentRepository.findAllByPatient(patient)
                .stream()
                .map(appointmentMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * Find all by start time between page.
     *
     * @param firstTime the first time
     * @param lastTime  the last time
     * @return the page
     */
    @Override
    public List<AppointmentDTO> findAllByStartTimeBetween(Instant firstTime,
                                                          Instant lastTime) {
        return appointmentRepository.findAllByStartTimeBetween(firstTime, lastTime)
                .stream()
                .map(appointmentMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * Find all by finish time between page.
     *
     * @param firstTime the first time
     * @param lastTime  the last time
     * @return the page
     */
    @Override
    public List<AppointmentDTO> findAllByFinishTimeBetween(Instant firstTime,
                                                           Instant lastTime) {
        return appointmentRepository.findAllByFinishTimeBetween(firstTime, lastTime)
                .stream()
                .map(appointmentMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * Find all by department page.
     *
     * @param nurseDTO the nurse dto
     * @return the page
     */
    @Override
    public List<AppointmentDTO> findAllByDepartment(NurseDTO nurseDTO) {
        Department department = departmentMapper.toEntityFromId(nurseDTO.getDepartmentId());

        return appointmentRepository.findAllByDepartment(department)
                .stream()
                .map(appointmentMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<AppointmentDTO> findById(Long id) {
        return appointmentRepository.findById(id)
                .map(appointmentMapper::toDto);
    }

    @Override
    public boolean existsById(Long id) {
        return appointmentRepository.existsById(id);
    }

    @Override
    public void deleteById(Long id) {
        appointmentRepository.deleteById(id);
    }

}
