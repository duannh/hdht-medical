package edu.poly.medical.hdht.common;

import edu.poly.medical.hdht.service.dto.PasswordGenerator;

/**
 * The type String utils.
 */
public class StringUtils {

    /**
     * utf 8 encoding value
     */
    public static final String ENCODING_UTF_8 = "UTF-8";

    /**
     * Generate passay password string.
     *
     * @return the string
     */
    public static String generatePassayPassword() {
        PasswordGenerator passwordGenerator = new PasswordGenerator.PasswordGeneratorBuilder()
                .useDigits(true)
                .useLower(true)
                .useUpper(true)
                .build();

        return passwordGenerator.generate(8); // output ex.: lrU12fmM 75iwI90o
    }
}
