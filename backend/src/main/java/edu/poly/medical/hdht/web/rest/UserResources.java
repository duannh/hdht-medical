package edu.poly.medical.hdht.web.rest;

import edu.poly.medical.hdht.service.UserService;
import edu.poly.medical.hdht.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static edu.poly.medical.hdht.common.ResourcesConstants.*;

/**
 * The type User resources.
 */
@RestController
@RequestMapping(RESOURCE_API)
public class UserResources {

    @Autowired
    private UserService userService;

    /**
     * Gets all users.
     *
     * @param page the page
     * @param size the size
     * @return the all users
     */
    @GetMapping(USER_MAPPING)
    public ResponseEntity<List<UserDTO>> getAllUsers(
            @RequestParam("page") int page, @RequestParam("size") int size) {
        Sort sort = new Sort(new Sort.Order(Sort.Direction.ASC, "lastName"));
        Pageable pageable = new PageRequest(page, size, sort);

        Page<UserDTO> Users = userService.findAll(pageable);

        return ResponseEntity.ok(Users.getContent());
    }

    /**
     * Create user response entity.
     *
     * @param dto the dto
     * @return the response entity
     */
    @PostMapping(USER_MAPPING)
    public ResponseEntity<UserDTO> createUser(@Valid @RequestBody UserDTO dto) {
        if (dto.getId() != null) {
            return ResponseEntity.badRequest().build();
        }

        return Optional.ofNullable(userService.save(dto))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Update user response entity.
     *
     * @param dto the dto
     * @return the response entity
     */
    @PutMapping(USER_MAPPING)
    public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UserDTO dto) {
        if (dto.getId() == null) {
            return ResponseEntity.badRequest().build();
        }

        return Optional.ofNullable(userService.save(dto))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Gets user by id.
     *
     * @param id the id
     * @return the user by id
     */
    @GetMapping(USER_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<UserDTO> getUserById(@PathVariable Long id) {
        return userService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Delete user by id response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping(USER_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<Void> deleteUserById(@PathVariable Long id) {
        userService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}