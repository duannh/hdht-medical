package edu.poly.medical.hdht.service;

import edu.poly.medical.hdht.service.dto.BlogDTO;

import java.util.List;
import java.util.Optional;

public interface BlogService {

    List<BlogDTO> findAll();

    BlogDTO save(BlogDTO dto);

    Optional<BlogDTO> findById(Long aLong);

    void deleteById(Long aLong);
     BlogDTO findBlogById(Long id);

}
