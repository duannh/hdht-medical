package edu.poly.medical.hdht.service.dto;

import lombok.Data;

import java.util.List;

/**
 * The type Patient dto.
 */
@Data
public class PatientDTO extends UserDTO {

    private Long id;

    private String cardId;

    private String insuranceCardNumber;

    private Long userId;

    private List<AppointmentDTO> appointments;

}
