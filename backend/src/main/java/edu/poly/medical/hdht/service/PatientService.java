package edu.poly.medical.hdht.service;

import edu.poly.medical.hdht.service.dto.PatientDTO;
import edu.poly.medical.hdht.service.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * The interface Patient service.
 */
public interface PatientService {

    /**
     * Find all page.
     *
     * @param pageable the pageable
     * @return the page
     */
    Page<PatientDTO> findAll(Pageable pageable);

    /**
     * Find all list.
     *
     * @return the list
     */
    List<PatientDTO> findAll();

    /**
     * Save patient dto.
     *
     * @param dto the dto
     * @return the patient dto
     */
    PatientDTO save(PatientDTO dto);

    /**
     * Find by id optional.
     *
     * @param aLong the a long
     * @return the optional
     */
    Optional<PatientDTO> findById(Long aLong);

    /**
     * Exists by id boolean.
     *
     * @param aLong the a long
     * @return the boolean
     */
    boolean existsById(Long aLong);

    /**
     * Delete by id.
     *
     * @param aLong the a long
     */
    void deleteById(Long aLong);

    /**
     * Find by user optional.
     *
     * @param user the user
     * @return the optional
     */
    Optional<PatientDTO> findByUser(UserDTO user);

}
