package edu.poly.medical.hdht.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * The type Appointment.
 */
@Data
@Entity
@Table(name = "hdht_appointment")
public class Appointment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * thoi gian gui request cuoc hen
     */
    @Column(name = "time_registered")
    private Instant timeRegistered;

    /**
     * lý do khám
     */
    @Column(name = "reason")
    private String reason;

    @Column(name = "appointment_code", unique = true, nullable = false)
    private String appointmentCode;

    /**
     * ca kham
     */
    @Column(name = "shift")
    private Integer shift;

    @Column(name = "start_time")
    private Instant startTime;

    @Column(name = "finish_time")
    private Instant finishTime;

    @Column(name = "location")
    private String location;

    @Column(name = "status")
    private Integer status;

    @Column(name = "note")
    private String note;

    @ManyToOne(targetEntity = Department.class)
    @JoinColumn(name = "department_id")
    private Department department;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "create_by", nullable = true)
    private User createdBy;

    @ManyToOne(targetEntity = Doctor.class)
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @ManyToOne(targetEntity = Patient.class)
    @JoinColumn(name = "patient_id")
    private Patient patient;

}
