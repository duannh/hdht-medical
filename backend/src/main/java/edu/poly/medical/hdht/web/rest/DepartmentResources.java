package edu.poly.medical.hdht.web.rest;

import edu.poly.medical.hdht.service.DepartmentService;
import edu.poly.medical.hdht.service.DoctorService;
import edu.poly.medical.hdht.service.dto.DepartmentDTO;
import edu.poly.medical.hdht.service.dto.DoctorDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static edu.poly.medical.hdht.common.ResourcesConstants.*;

/**
 * The type Department resources.
 */
@RestController
@RequestMapping(RESOURCE_API)
public class DepartmentResources {

    @Autowired
    private DepartmentService departmentService;

    /**
     * GET  /products : get all the departments.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of entity in body
     */
    @GetMapping(DEPARTMENT_MAPPING)
    public ResponseEntity<List<DepartmentDTO>> getAllDepartments() {
        List<DepartmentDTO> departments = departmentService.findAll();
        return ResponseEntity.ok(departments);
    }

    /**
     * POST  /departments : Create a new department.
     *
     * @param dto the dto to create
     * @return the ResponseEntity with status 201 (Created) and with body the new entity, or with status 400 (Bad Request) if the entity has already an ID
     */
    @PostMapping(DEPARTMENT_MAPPING)
    public ResponseEntity<DepartmentDTO> createDepartment(@Valid @RequestBody DepartmentDTO dto) {
        if (dto.getId() != null) {
            return ResponseEntity.badRequest().build();
        }

        return Optional.ofNullable(departmentService.save(dto))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * PUT  /departments : Updates an existing department.
     *
     * @param dto the dto to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated entity, or with status 400 (Bad Request) if the entity is not valid, or with status 500 (Internal Server Error) if the entity couldn't be updated
     */
    @PutMapping(DEPARTMENT_MAPPING)
    public ResponseEntity<DepartmentDTO> updateDepartment(@Valid @RequestBody DepartmentDTO dto) {
        if (dto.getId() == null) {
            return ResponseEntity.badRequest().build();
        }

        return Optional.ofNullable(departmentService.save(dto))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * GET  /departments/:id : get the "id" department.
     *
     * @param id the id of the department to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the entity, or with status 404 (Not Found)
     */
    @GetMapping(DEPARTMENT_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<DepartmentDTO> getDepartment(@PathVariable Long id) {
        return departmentService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * DELETE  /departments/:id : delete the "id" department.
     *
     * @param id the id of the entity to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping(DEPARTMENT_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<Void> deleteDepartment(@PathVariable Long id) {
        departmentService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}