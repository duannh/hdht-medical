package edu.poly.medical.hdht.service.impl;

import edu.poly.medical.hdht.domain.MedicalHistory;
import edu.poly.medical.hdht.repository.MedicalHistoryRepository;
import edu.poly.medical.hdht.service.MedicalHistoryService;
import edu.poly.medical.hdht.service.dto.MedicalHistoryDTO;
import edu.poly.medical.hdht.service.mapper.MedicalHistoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The type Medical history service.
 */
@Service
public class MedicalHistoryServiceImpl implements MedicalHistoryService {

    @Autowired
    private MedicalHistoryMapper medicalHistoryMapper;

    @Autowired
    private MedicalHistoryRepository medicalHistoryRepository;

    /**
     * Returns a {@link Page} of entities meeting the paging restriction provided in the {@code Pageable} object.
     *
     * @param pageable
     * @return a page of entities
     */
    @Override
    public Page<MedicalHistoryDTO> findAll(Pageable pageable) {
        return medicalHistoryRepository.findAll(pageable).map(medicalHistoryMapper::toDto);
    }

    @Override
    public List<MedicalHistoryDTO> findAll() {
        return medicalHistoryRepository.findAll().stream()
                .map(medicalHistoryMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed the
     * entity instance completely.
     *
     * @param dto must not be {@literal null}.
     * @return the saved entity; will never be {@literal null}.
     */
    @Override
    public MedicalHistoryDTO save(MedicalHistoryDTO dto) {
        MedicalHistory entity = medicalHistoryMapper.toEntity(dto);

        return Optional.of(medicalHistoryRepository.save(entity)).map(medicalHistoryMapper::toDto).orElse(null);
    }

    /**
     * Retrieves an entity by its id.
     *
     * @param aLong must not be {@literal null}.
     * @return the entity with the given id or {@literal Optional#empty()} if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    public Optional<MedicalHistoryDTO> findById(Long aLong) {
        return medicalHistoryRepository.findById(aLong).map(medicalHistoryMapper::toDto);
    }

    /**
     * Returns whether an entity with the given id exists.
     *
     * @param aLong must not be {@literal null}.
     * @return {@literal true} if an entity with the given id exists, {@literal false} otherwise.
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    public boolean existsById(Long aLong) {
        return medicalHistoryRepository.existsById(aLong);
    }

    /**
     * Deletes the entity with the given id.
     *
     * @param aLong must not be {@literal null}.
     * @throws IllegalArgumentException in case the given {@code id} is {@literal null}
     */
    @Override
    public void deleteById(Long aLong) {
        medicalHistoryRepository.deleteById(aLong);
    }

}
