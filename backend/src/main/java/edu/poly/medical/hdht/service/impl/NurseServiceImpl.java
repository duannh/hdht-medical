package edu.poly.medical.hdht.service.impl;

import edu.poly.medical.hdht.domain.Nurse;
import edu.poly.medical.hdht.domain.User;
import edu.poly.medical.hdht.repository.NurseRepository;
import edu.poly.medical.hdht.service.NurseService;
import edu.poly.medical.hdht.service.UserService;
import edu.poly.medical.hdht.service.dto.NurseDTO;
import edu.poly.medical.hdht.service.dto.UserDTO;
import edu.poly.medical.hdht.service.mapper.NurseMapper;
import edu.poly.medical.hdht.service.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The type Nurse service.
 */
@Service
public class NurseServiceImpl implements NurseService {

    @Autowired
    private NurseMapper nurseMapper;

    @Autowired
    private NurseRepository nurseRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;

    /**
     * Returns a {@link Page} of entities meeting the paging restriction provided in the {@code Pageable} object.
     *
     * @return a page of entities
     */

    @Override
    public List<NurseDTO> findAll() {
        return nurseRepository.findAll().stream()
                .map(nurseMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed the
     * entity instance completely.
     *
     * @param dto must not be {@literal null}.
     * @return the saved entity; will never be {@literal null}.
     */
    @Override
    public NurseDTO save(NurseDTO dto) {
        User user = userService.save(dto);
        Nurse entity = nurseMapper.toEntity(dto);
        entity.setUser(user);

        return Optional.ofNullable(nurseRepository.save(entity))
                .map(nurseMapper::toDto)
                .orElse(null);
    }

    /**
     * Retrieves an entity by its id.
     *
     * @param aLong must not be {@literal null}.
     * @return the entity with the given id or {@literal Optional#empty()} if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    public Optional<NurseDTO> findById(Long aLong) {
        return nurseRepository.findById(aLong).map(nurseMapper::toDto);
    }

    /**
     * Returns whether an entity with the given id exists.
     *
     * @param aLong must not be {@literal null}.
     * @return {@literal true} if an entity with the given id exists, {@literal false} otherwise.
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    public boolean existsById(Long aLong) {
        return nurseRepository.existsById(aLong);
    }

    /**
     * Deletes the entity with the given id.
     *
     * @param aLong must not be {@literal null}.
     * @throws IllegalArgumentException in case the given {@code id} is {@literal null}
     */
    @Override
    public void deleteById(Long aLong) {
        nurseRepository.deleteById(aLong);
    }

    /**
     * Retrieves an entity by its user.
     *
     * @param userDTO must not be {@literal null}.
     * @return the entity with the given id or {@literal Optional#empty()} if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    public Optional<NurseDTO> findByUser(UserDTO userDTO) {
        User user = userMapper.toEntity(userDTO);
        return nurseRepository.findByUser(user)
                .map(nurseMapper::toDto);
    }
}
