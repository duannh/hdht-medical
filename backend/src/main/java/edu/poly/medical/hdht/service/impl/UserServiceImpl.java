package edu.poly.medical.hdht.service.impl;

import edu.poly.medical.hdht.domain.Authority;
import edu.poly.medical.hdht.domain.User;
import edu.poly.medical.hdht.repository.AuthorityRepository;
import edu.poly.medical.hdht.repository.UserRepository;
import edu.poly.medical.hdht.service.UserService;
import edu.poly.medical.hdht.service.dto.DoctorDTO;
import edu.poly.medical.hdht.service.dto.NurseDTO;
import edu.poly.medical.hdht.service.dto.PatientDTO;
import edu.poly.medical.hdht.service.dto.UserDTO;
import edu.poly.medical.hdht.service.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static edu.poly.medical.hdht.common.AuthoritiesConstants.*;
import static edu.poly.medical.hdht.common.UserConstants.ACTIVE_ACCOUNT;
import static edu.poly.medical.hdht.common.UserConstants.IN_ACTIVE_ACCOUNT;

/**
 * The type User service.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthorityRepository authorityRepository;

    /**
     * Returns a {@link Page} of entities meeting the paging restriction provided in the {@code Pageable} object.
     *
     * @param pageable
     * @return a page of entities
     */
    @Override
    public Page<UserDTO> findAll(Pageable pageable) {
        return userRepository.findAll(pageable).map(userMapper::toDto);
    }


    /**
     * Exists by phone boolean.
     *
     * @param phone the phone
     * @return the boolean
     */
    @Override
    public boolean existsByPhone(String phone) {
        return userRepository.existsByPhone(phone);
    }

    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed the
     * entity instance completely.
     *
     * @param dto must not be {@literal null}.
     * @return the saved entity; will never be {@literal null}.
     */
    @Override
    public UserDTO save(UserDTO dto) {
        User entity = userMapper.toEntity(dto);

        return Optional.ofNullable(userRepository.save(entity)).map(userMapper::toDto).orElse(null);
    }

    /**
     * Save user dto.
     *
     * @param dto the dto
     * @return the user dto
     */
    @Override
    public User save(DoctorDTO dto) {
        User user = new User();
        if (dto.getId() == null) {
            user.setId(dto.getUserId());
            user.setCreatedDate(Instant.now());
            user.setActivated(IN_ACTIVE_ACCOUNT);
            user.setAddress(dto.getAddress());

            List<Authority> authorities = new ArrayList<>();
            authorities.add(authorityRepository.findByName(AUTHORITY_DOCTOR));
            authorities.add(authorityRepository.findByName(AUTHORITY_USER));

            user.setAuthorities(authorities);
            user.setBirthday(dto.getBirthday());
            user.setEmail(dto.getEmail());
            user.setImageUrl(dto.getImageUrl());
            user.setPhone("0".concat(dto.getPhone()));
            user.setGender(dto.getGender());
            user.setFirstName(dto.getFirstName());
            user.setLastName(dto.getLastName());
            user.setPassword(dto.getPassword());
        } else {
            user = userRepository.findById(dto.getUserId()).get();
            user.setLastName(dto.getLastName());
            user.setFirstName(dto.getFirstName());
            user.setGender(dto.getGender());
            user.setAddress(dto.getAddress());
            user.setEmail(dto.getEmail());
            user.setLastUpdatedDate(Instant.now());
        }

        return userRepository.save(user);
    }

    /**
     * Save user.
     *
     * @param dto the dto
     * @return the user
     */
    @Override
    public User save(NurseDTO dto) {
        User user = new User();
        if (dto.getId() == null) {
            user.setId(dto.getUserId());
            user.setCreatedDate(Instant.now());
            user.setActivated(IN_ACTIVE_ACCOUNT);
            user.setAddress(dto.getAddress());

            List<Authority> authorities = new ArrayList<>();
            authorities.add(authorityRepository.findByName(AUTHORITY_NURSE));
            authorities.add(authorityRepository.findByName(AUTHORITY_USER));

            user.setAuthorities(authorities);
            user.setBirthday(dto.getBirthday());
            user.setEmail(dto.getEmail());
            user.setImageUrl(dto.getImageUrl());
            user.setPhone("0".concat(dto.getPhone()));
            user.setGender(dto.getGender());
            user.setFirstName(dto.getFirstName());
            user.setLastName(dto.getLastName());
            user.setPassword(dto.getPassword());
        } else {
            user = userRepository.findById(dto.getUserId()).get();
            user.setLastName(dto.getLastName());
            user.setFirstName(dto.getFirstName());
            user.setGender(dto.getGender());
            user.setAddress(dto.getAddress());
            user.setEmail(dto.getEmail());
            user.setLastUpdatedDate(Instant.now());
        }

        return userRepository.save(user);
    }

    /**
     * Save user dto.
     *
     * @param dto the dto
     * @return the user dto
     */
    @Override
    public User save(PatientDTO dto) {
        User user = new User();
        if (dto.getId() == null) {
            user.setId(dto.getUserId());
            user.setCreatedDate(Instant.now());
            user.setActivated(IN_ACTIVE_ACCOUNT);
            user.setAddress(dto.getAddress());

            List<Authority> authorities = new ArrayList<>();
            authorities.add(authorityRepository.findByName(AUTHORITY_PATIENT));
            authorities.add(authorityRepository.findByName(AUTHORITY_USER));

            user.setAuthorities(authorities);
            user.setBirthday(dto.getBirthday());
            user.setEmail(dto.getEmail());
            user.setImageUrl(dto.getImageUrl());
            user.setPhone("0".concat(dto.getPhone()));
            user.setGender(dto.getGender());
            user.setFirstName(dto.getFirstName());
            user.setLastName(dto.getLastName());
            user.setPassword(dto.getPassword());
        } else {
            user = userRepository.findById(dto.getUserId()).get();
            user.setLastName(dto.getLastName());
            user.setFirstName(dto.getFirstName());
            user.setGender(dto.getGender());
            user.setAddress(dto.getAddress());
            user.setEmail(dto.getEmail());
            user.setLastUpdatedDate(Instant.now());
        }

        return userRepository.save(user);
    }

    /**
     * Retrieves an entity by its id.
     *
     * @param aLong must not be {@literal null}.
     * @return the entity with the given id or {@literal Optional#empty()} if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    public Optional<UserDTO> findById(Long aLong) {
        return userRepository.findById(aLong).map(userMapper::toDto);
    }

    /**
     * Retrieves an entity by its phone.
     *
     * @param phone must not be {@literal null}.
     * @return the entity with the given phone or {@literal Optional#empty()} if none found
     * @throws IllegalArgumentException if {@code phone} is {@literal null}.
     */
    @Override
    public UserDTO findByPhone(String phone) {
        return Optional.ofNullable(userRepository.findByPhone(phone)).map(userMapper::toDto).orElse(null);
    }

    /**
     * Returns whether an entity with the given id exists.
     *
     * @param aLong must not be {@literal null}.
     * @return {@literal true} if an entity with the given id exists, {@literal false} otherwise.
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    public boolean existsById(Long aLong) {
        return userRepository.existsById(aLong);
    }

    /**
     * Deletes the entity with the given id.
     *
     * @param aLong must not be {@literal null}.
     * @throws IllegalArgumentException in case the given {@code id} is {@literal null}
     */
    @Override
    public void deleteById(Long aLong) {
        userRepository.deleteById(aLong);
    }

    /**
     * Active user by id user dto.
     *
     * @param id the id
     * @return the user dto
     */
    @Override
    public UserDTO activeUserById(Long id) {
        User user = userRepository.findById(id).get();
        user.setActivated(ACTIVE_ACCOUNT);

        return Optional.ofNullable(userRepository.save(user))
                .map(userMapper::toDto)
                .orElse(null);
    }

}
