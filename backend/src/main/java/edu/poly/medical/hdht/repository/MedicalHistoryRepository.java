package edu.poly.medical.hdht.repository;

import edu.poly.medical.hdht.domain.MedicalHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface Medical history repository.
 */
@Repository
public interface MedicalHistoryRepository extends JpaRepository<MedicalHistory, Long> {
}
