package edu.poly.medical.hdht.web.rest;

import edu.poly.medical.hdht.service.AppointmentService;
import edu.poly.medical.hdht.service.DoctorService;
import edu.poly.medical.hdht.service.NurseService;
import edu.poly.medical.hdht.service.PatientService;
import edu.poly.medical.hdht.service.dto.AppointmentDTO;
import edu.poly.medical.hdht.service.dto.DoctorDTO;
import edu.poly.medical.hdht.service.dto.PatientDTO;
import edu.poly.medical.hdht.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static edu.poly.medical.hdht.common.Constants.APPOINTMENT_REQUEST;
import static edu.poly.medical.hdht.common.ResourcesConstants.*;
import static edu.poly.medical.hdht.common.SecurityUtils.*;

/**
 * The type Appointment resources.
 */
@RestController
@RequestMapping(RESOURCE_API)
public class AppointmentResources {

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private AccountResources accountResources;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private NurseService nurseService;

    @Autowired
    private PatientService patientService;

    /**
     * Gets all appointments.
     *
     * @return the all appointments
     */
    @GetMapping(APPOINTMENT_MAPPING)
    public ResponseEntity<List<AppointmentDTO>> getAllAppointments() {
        UserDTO userDTO = accountResources.getCurrentUser();
        List<AppointmentDTO> appointments = null;
        if (isAdmin()) {
            appointments = appointmentService.findAll();
        } else if (isNurse()) {
            appointments = appointmentService.findAllByCreatedBy(userDTO);
        } else if (isDoctor()) {
            DoctorDTO doctorDTO = doctorService.findByUser(userDTO).get();
            appointments = appointmentService.findAllByDoctor(doctorDTO);
        } else if (isPatient()) {
            PatientDTO patientDTO = patientService.findByUser(userDTO).get();
            appointments = appointmentService.findAllByPatient(patientDTO);
        }

        return ResponseEntity.ok(appointments);
    }

    /**
     * Gets all appointments but request.
     *
     * @return the all appointments but request
     */
    @GetMapping(APPOINTMENT_REQUEST_MAPPING)
    public ResponseEntity<List<AppointmentDTO>> getAllAppointmentsButRequest() {
        List<AppointmentDTO> appointments = null;

        if (isNurse()) {
            appointments = appointmentService.findAllByStatusIsRequest();
        }

        return ResponseEntity.ok(appointments);
    }

    /**
     * Create appointment response entity.
     *
     * @param dto the dto
     * @return the response entity
     */
    @PostMapping(APPOINTMENT_MAPPING)
    public ResponseEntity<AppointmentDTO> createAppointment(@Valid @RequestBody AppointmentDTO dto) {
        if (dto.getId() != null) {
            return ResponseEntity.badRequest().build();
        }
        dto.setCreatedBy(accountResources.getCurrentUser().getId());
        dto.setTimeRegistered(Instant.now());
        dto.setAppointmentCode("HDT-".concat(String.valueOf(dto.getTimeRegistered().toEpochMilli()).substring(0, 10)).concat(dto.getCreatedBy().toString().substring(0, 1)));
        dto.setStatus(APPOINTMENT_REQUEST);

        return Optional.ofNullable(appointmentService.save(dto))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Update appointment response entity.
     *
     * @param dto the dto
     * @return the response entity
     */
    @PutMapping(APPOINTMENT_MAPPING)
    public ResponseEntity<AppointmentDTO> updateAppointment(@Valid @RequestBody AppointmentDTO dto) {
        if (dto.getId() == null) {
            return ResponseEntity.badRequest().build();
        }

        return Optional.ofNullable(appointmentService.save(dto))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Gets appointment by id.
     *
     * @param id the id
     * @return the appointment by id
     */
    @GetMapping(APPOINTMENT_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<AppointmentDTO> getAppointmentById(@PathVariable Long id) {
        return appointmentService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Delete appointment by id response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping(APPOINTMENT_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<Void> deleteAppointmentById(@PathVariable Long id) {
        appointmentService.deleteById(id);
        return ResponseEntity.ok().build();
    }
    @PostMapping("/addAppoiment")
    public ResponseEntity<AppointmentDTO> addAppointment(@Valid @RequestBody AppointmentDTO dto) {
        if (dto.getId() != null) {
            return ResponseEntity.badRequest().build();
        }
        return Optional.ofNullable(appointmentService.save(dto))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }
}
