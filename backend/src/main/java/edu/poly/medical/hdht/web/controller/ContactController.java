package edu.poly.medical.hdht.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The type Contact controller.
 */
@Controller
@RequestMapping("/contacts")
public class ContactController {

    /**
     * Go to service page string.
     *
     * @return the string
     */
    @GetMapping(value = {"/", ""})
    public String goToServicePage() {
        return "contact";
    }

}
