package edu.poly.medical.hdht.config;

import edu.poly.medical.hdht.domain.*;
import edu.poly.medical.hdht.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static edu.poly.medical.hdht.common.AuthoritiesConstants.*;
import static edu.poly.medical.hdht.common.UserConstants.ACTIVE_ACCOUNT;
import static edu.poly.medical.hdht.common.UserConstants.GENDER_MALE;

/**
 * The type Data seeding listener.
 */
@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private NurseRepository nurseRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent arg0) {

        Department department;

        if (!departmentRepository.existsByName("noi")) {
            Department d = new Department();
            d.setName("noi");

            department = departmentRepository.save(d);
        } else {
            department = departmentRepository.findByName("noi");
        }
        if (!authorityRepository.existsByName(AUTHORITY_ADMIN)) {
            authorityRepository.save(new Authority(AUTHORITY_ADMIN));
        }

        if (!authorityRepository.existsByName(AUTHORITY_DOCTOR)) {
            authorityRepository.save(new Authority(AUTHORITY_DOCTOR));
        }

        if (!authorityRepository.existsByName(AUTHORITY_NURSE)) {
            authorityRepository.save(new Authority(AUTHORITY_NURSE));
        }

        if (!authorityRepository.existsByName(AUTHORITY_PATIENT)) {
            authorityRepository.save(new Authority(AUTHORITY_PATIENT));
        }

        if (!authorityRepository.existsByName(AUTHORITY_USER)) {
            authorityRepository.save(new Authority(AUTHORITY_USER));
        }

        if (!authorityRepository.existsByName(AUTHORITY_ANONYMOUS)) {
            authorityRepository.save(new Authority(AUTHORITY_ANONYMOUS));
        }
        if (null == userRepository.findByPhone("0389185187")) {
            User user = new User();
            user.setPhone("0389185187");
            user.setPassword(passwordEncoder.encode("admin"));
            user.setFirstName("Duan");
            user.setLastName("Huu Ngo");
            user.setGender(GENDER_MALE);
            user.setEmail("duannhqb@gmail.com");
            user.setAddress("38 Đặng Minh Khiêm");
            user.setBirthday(Instant.now());
            user.setCreatedDate(Instant.now());
            user.setActivated(ACTIVE_ACCOUNT);
            List<Authority> authorities = new ArrayList<>();
            authorities.add(authorityRepository.findByName(AUTHORITY_ADMIN));
            authorities.add(authorityRepository.findByName(AUTHORITY_USER));
            user.setAuthorities(authorities);
            userRepository.save(user);
        }
    }
}