package edu.poly.medical.hdht.repository;

import edu.poly.medical.hdht.domain.Nurse;
import edu.poly.medical.hdht.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The interface Nurse repository.
 */
@Repository
public interface NurseRepository extends JpaRepository<Nurse, Long> {

    /**
     * Find by user optional.
     *
     * @param user the user
     * @return the optional
     */
    Optional<Nurse> findByUser(User user);

}
