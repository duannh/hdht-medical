package edu.poly.medical.hdht.repository;

import edu.poly.medical.hdht.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

/**
 * The interface Appointment repository.
 */
@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

    /**
     * Find all by status page.
     *
     * @param status the status
     * @return the page
     */
    List<Appointment> findAllByStatus(Integer status);

    /**
     * Find all by nurse page.
     *
     * @param user the user
     * @return the page
     */
    List<Appointment> findAllByCreatedBy(User user);

    /**
     * Find all by department page.
     *
     * @param department the department
     * @return the page
     */
    List<Appointment> findAllByDepartment(Department department);

    /**
     * Find all by nurse and department page.
     *
     * @param user       the user
     * @param department the department
     * @return the page
     */
    List<Appointment> findAllByCreatedByAndDepartment(User user, Department department);

    /**
     * Find all by doctor page.
     *
     * @param doctor the doctor
     * @return the page
     */
    List<Appointment> findAllByDoctor(Doctor doctor);

    /**
     * Find all by patient page.
     *
     * @param patient the patient
     * @return the page
     */
    List<Appointment> findAllByPatient(Patient patient);

    /**
     * Find all by start time between page.
     *
     * @param firstTime the first time
     * @param lastTime  the last time
     * @return the page
     */
    List<Appointment> findAllByStartTimeBetween(Instant firstTime, Instant lastTime);

    /**
     * Find all by finish time between page.
     *
     * @param firstTime the first time
     * @param lastTime  the last time
     * @return the page
     */
    List<Appointment> findAllByFinishTimeBetween(Instant firstTime, Instant lastTime);

}
