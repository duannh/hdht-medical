package edu.poly.medical.hdht.web.controller;

import edu.poly.medical.hdht.common.StringUtils;
import edu.poly.medical.hdht.common.TwilioSms;
import edu.poly.medical.hdht.service.PatientService;
import edu.poly.medical.hdht.service.UserService;
import edu.poly.medical.hdht.service.dto.PatientDTO;
import edu.poly.medical.hdht.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private UserService userService;
    @Autowired
    private PatientService patientService;

    @GetMapping(value = {"/register"})
    public String goToRegister(ModelMap model) {
        model.addAttribute("patientDTO", new PatientDTO());
        return "register";
    }

    @PostMapping(value = {"/register"})
    public String register(PatientDTO dto,BindingResult bindingResult, ModelMap model) {
        PatientDTO patientDTO = new PatientDTO();
        String password = StringUtils.generatePassayPassword();
        patientDTO.setFirstName(dto.getFirstName());
        patientDTO.setLastName(dto.getLastName());
        patientDTO.setPassword(dto.getPassword());
        patientDTO.setCardId(dto.getCardId());
        patientDTO.setInsuranceCardNumber(dto.getInsuranceCardNumber());
        patientDTO.setPhone(dto.getPhone());
        patientDTO.setEmail(dto.getEmail());
        if (patientDTO.getId() != null) {
            String body = TwilioSms.createAccount(password);
            TwilioSms.sendMessage(dto.getPhone(), body);
        }
        patientService.save(patientDTO);
        return "/register";
    }

    @GetMapping(value = {"/forgot"})
    public String goToForgot(ModelMap model) {
       /* model.addAttribute("patientDTO", new PatientDTO());*/
        return "forgot-password";
    }


}
