package edu.poly.medical.hdht.web.controller;

import edu.poly.medical.hdht.service.DepartmentService;
import edu.poly.medical.hdht.service.DoctorService;
import edu.poly.medical.hdht.service.dto.DoctorDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * The type Doctor controller.
 */
@Controller
@RequestMapping("/doctors")
public class DoctorController {
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private DepartmentService departmentService;
    /**
     * Go to service page string.
     *
     * @return the string
     */
    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String listDoctor(ModelMap model) {
        model.addAttribute("doctors", doctorService.findAll());
        model.addAttribute("department",departmentService.findAll());
        return "doctors";
    }

//    @GetMapping("/")
//    public String showDoctor(Model model, HttpServletRequest request, RedirectAttributes redirect) {
//        request.getSession().setAttribute("doctorList", null);
//        return "redirect:/doctors/page/1";
//    }
//
//    @GetMapping("/doctors/page/{pageNumber}")
//    public String showDoctorPage(HttpServletRequest request, @PathVariable int pageNumber, Model model) {
//        PagedListHolder<?> pages = (PagedListHolder<?>) request.getSession().getAttribute("doctorList");
//        int pagesize = 8;
//        List<DoctorDTO> list =  doctorService.findAll();
//        if (pages == null) {
//            pages = new PagedListHolder<>(list);
//            pages.setPageSize(pagesize);
//        } else {
//            final int goToPage = pageNumber - 1;
//            if (goToPage <= pages.getPageCount() && goToPage >= 0) {
//                pages.setPage(goToPage);
//            }
//        }
//        request.getSession().setAttribute("doctorList", pages);
//        int current = pages.getPage() + 1;
//        int begin = Math.max(1, current - list.size());
//        int end = Math.min(begin + 5, pages.getPageCount());
//        int totalPageCount = pages.getPageCount();
//        String baseUrl = "/doctors/page/";
//
//        model.addAttribute("beginIndex", begin);
//        model.addAttribute("endIndex", end);
//        model.addAttribute("currentIndex", current);
//        model.addAttribute("totalPageCount", totalPageCount);
//        model.addAttribute("baseUrl", baseUrl);
//        model.addAttribute("doctorst", pages);
//        return "doctors";
//    }


}
