package edu.poly.medical.hdht.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The type Authority.
 */
@Data
@Entity
@Table(name = "hdht_authority")
@NoArgsConstructor
public class Authority implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "authority_name", nullable = false, unique = true)
    private String name;

    /**
     * Instantiates a new Authority.
     *
     * @param name the name
     */
    public Authority(String name) {
        this.name = name;
    }

}
