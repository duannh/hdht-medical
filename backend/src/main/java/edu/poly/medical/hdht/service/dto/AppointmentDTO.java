package edu.poly.medical.hdht.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.Instant;

/**
 * The type Appointment dto.
 */
@Data
public class AppointmentDTO {

    private Long id;

    @JsonProperty("appointmentCode")
    private String appointmentCode;

    /**
     * thoi gian gui request cuoc hen
     */
    private Instant timeRegistered;

    /**
     * ca kham
     */
    private Integer shift;

    private String reason;

    private Instant startTime;

    private Instant finishTime;

    private String location;

    private Integer status;

    private String note;

    private Long departmentId;

    private String departmentName;

    private Long createdBy;

    private UserDTO createdByUser;

    private DoctorDTO doctorDTO;

    private PatientDTO patientDTO;

    private Long patientId;

    private Long doctorId;

}
