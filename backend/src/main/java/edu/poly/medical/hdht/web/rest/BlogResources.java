package edu.poly.medical.hdht.web.rest;

import edu.poly.medical.hdht.service.BlogService;
import edu.poly.medical.hdht.service.dto.BlogDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static edu.poly.medical.hdht.common.ResourcesConstants.*;

@RestController
@RequestMapping(RESOURCE_API)
public class BlogResources {
    @Autowired
    private BlogService blogService;

    @GetMapping(BLOG_MAPPING)
     public ResponseEntity<List<BlogDTO>> getAllDepartments() {
        List<BlogDTO> blog = blogService.findAll();
        return ResponseEntity.ok(blog);
    }

    @PostMapping(BLOG_MAPPING)
    public List<BlogDTO> createBlog(@RequestBody BlogDTO dto) {
        this.blogService.save(dto);
        return this.blogService.findAll();
    }

    @PostMapping(BLOG_MAPPING + "/edit")
    public List<BlogDTO> editBlog(@RequestBody BlogDTO dto) {
        BlogDTO b = this.blogService.findBlogById(dto.getId());
        b.setName(dto.getName());
        b.setText(dto.getText());
        b.setImage(dto.getImage());
        this.blogService.save(b);
        return this.blogService.findAll();
    }

    @DeleteMapping(BLOG_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<Void> deleteBlogById(@PathVariable Long id) {
        blogService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
