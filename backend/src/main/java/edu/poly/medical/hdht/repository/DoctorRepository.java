package edu.poly.medical.hdht.repository;

import edu.poly.medical.hdht.domain.Doctor;
import edu.poly.medical.hdht.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * The interface Doctor repository.
 */
@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {

    /**
     * Find by user optional.
     *
     * @param user the user
     * @return the optional
     */
    Optional<Doctor> findByUser(User user);

    @Query(value = "select * from hdht_doctor where department_id = ? ", nativeQuery = true)
    List<Doctor> findADoctorByDepart(Long id);

    @Query(value = "SELECT * FROM medical.hdht_doctor inner join medical.hdht_user on hdht_doctor.id = hdht_user.id where hdht_user.last_name like N%?%", nativeQuery = true)
    List<Doctor> findSearchName(String name_doctor);
}
