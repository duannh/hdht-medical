package edu.poly.medical.hdht.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The type Medical history.
 */
@Data
@Entity
@Table(name = "hdht_medical_history")
public class MedicalHistory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "medical_information")
    private String information;

    @Column(name = "advice_from_doctor")
    private String adviceFromDoctor;

    @Column(name = "remedial")
    private String remedial;

    @Column(name = "height_body")
    private Integer height;

    @Column(name = "weight_body")
    private Integer weight;

    @Column(name = "blood_group")
    private Integer bloodGroup;

    @Column(name = "blood_pressure")
    private Integer bloodPressure;

    @Column(name = "temp_body")
    private Integer tempBody;

    /**
     * dau hieu benh
     */
    @Column(name = "signs_of_illness")
    private String signsOfIllness;

    @Column(name = "note")
    private String note;

    @OneToOne(targetEntity = Appointment.class)
    @JoinColumn(name = "appointment_id")
    private Appointment appointment;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "user_id")
    private User user;

}
