package edu.poly.medical.hdht.service.mapper;

import edu.poly.medical.hdht.domain.Nurse;
import edu.poly.medical.hdht.service.dto.NurseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The type Nurse mapper.
 */
@Component
public class NurseMapper implements EntityMapper<Nurse, NurseDTO> {

    @Autowired
    private AppointmentMapper appointmentMapper;

    @Autowired
    private DepartmentMapper departmentMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public Nurse toEntity(NurseDTO dto) {
        Nurse entity = new Nurse();
        entity.setId(dto.getId());
//        if (Optional.ofNullable(dto.getAppointments()).isPresent()) {
//            entity.setAppointments(appointmentMapper.toEntities(dto.getAppointments()));
//        }
        entity.setDepartment(departmentMapper.toEntityFromId(dto.getDepartmentId()));
        entity.setExperience(dto.getExperience());
        entity.setIntro(dto.getIntro());
        entity.setStudying(dto.getStudying());
        entity.setFunction(dto.getFunction());
        entity.setUser(userMapper.toEntityFromId(dto.getUserId()));

        return entity;
    }

    @Override
    public NurseDTO toDto(Nurse entity) {
        NurseDTO dto = new NurseDTO();
        dto.setId(entity.getId());
//        if (Optional.ofNullable(entity.getAppointments()).isPresent()) {
//            dto.setAppointments(appointmentMapper.toDTOs(entity.getAppointments()));
//        }
        dto.setDepartmentId(entity.getDepartment().getId());
        dto.setDepartmentName(entity.getDepartment().getName());
        dto.setUserId(entity.getUser().getId());
        dto.setIntro(entity.getIntro());
        dto.setStudying(entity.getStudying());
        dto.setExperience(entity.getExperience());
        dto.setFirstName(entity.getFunction());
        dto.setFunction(entity.getFunction());
        dto.setFirstName(entity.getUser().getFirstName());
        dto.setLastName(entity.getUser().getLastName());
        dto.setPhone(entity.getUser().getPhone());
        dto.setGender(entity.getUser().getGender());
        dto.setImageUrl(entity.getUser().getImageUrl());
        dto.setEmail(entity.getUser().getEmail());
        dto.setBirthday(entity.getUser().getBirthday());
        dto.setAddress(entity.getUser().getAddress());
        dto.setActivated(entity.getUser().getActivated());
        dto.setCreatedDate(entity.getUser().getCreatedDate());
        dto.setAuthorities(entity.getUser().getAuthorities());
        dto.setPassword(entity.getUser().getPassword());

        return dto;
    }

    @Override
    public List<NurseDTO> toDTOs(List<Nurse> entities) {
        return entities.stream()
                .filter(Objects::nonNull)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Nurse> toEntities(List<NurseDTO> dtos) {
        return dtos.stream()
                .filter(Objects::nonNull)
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

    @Override
    public Nurse toEntityFromId(Long id) {
        if (id == null) {
            return null;
        }
        Nurse nurse = new Nurse();
        nurse.setId(id);

        return nurse;
    }
}
