package edu.poly.medical.hdht.service.mapper;

import edu.poly.medical.hdht.domain.Patient;
import edu.poly.medical.hdht.service.dto.PatientDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The type Patient mapper.
 */
@Component
public class PatientMapper implements EntityMapper<Patient, PatientDTO> {

    @Autowired
    private AppointmentMapper appointmentMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public Patient toEntity(PatientDTO dto) {
        Patient entity = new Patient();
//        if (Optional.ofNullable(dto.getAppointments()).isPresent()) {
//            entity.setAppointments(appointmentMapper.toEntities(dto.getAppointments()));
//        }
        entity.setCardId(dto.getCardId());
        entity.setId(dto.getId());
        entity.setInsuranceCardNumber(dto.getInsuranceCardNumber());
        entity.setUser(userMapper.toEntityFromId(dto.getUserId()));

        return entity;
    }

    @Override
    public PatientDTO toDto(Patient entity) {
        PatientDTO dto = new PatientDTO();
//        if (Optional.ofNullable(entity.getAppointments()).isPresent()) {
//            dto.setAppointments(appointmentMapper.toDTOs(entity.getAppointments()));
//        }
        dto.setCardId(entity.getCardId());
        dto.setId(entity.getId());
        dto.setInsuranceCardNumber(entity.getInsuranceCardNumber());
        dto.setUserId(entity.getUser().getId());

        dto.setFirstName(entity.getUser().getFirstName());
        dto.setLastName(entity.getUser().getLastName());
        dto.setPhone(entity.getUser().getPhone());
        dto.setGender(entity.getUser().getGender());
        dto.setImageUrl(entity.getUser().getImageUrl());
        dto.setEmail(entity.getUser().getEmail());
        dto.setBirthday(entity.getUser().getBirthday());
        dto.setAddress(entity.getUser().getAddress());
        dto.setActivated(entity.getUser().getActivated());
        dto.setCreatedDate(entity.getUser().getCreatedDate());
        dto.setAuthorities(entity.getUser().getAuthorities());
        dto.setPassword(entity.getUser().getPassword());

        return dto;
    }

    @Override
    public List<PatientDTO> toDTOs(List<Patient> entities) {
        return entities.stream()
                .filter(Objects::nonNull)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Patient> toEntities(List<PatientDTO> dtos) {
        return dtos.stream()
                .filter(Objects::nonNull)
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

    @Override
    public Patient toEntityFromId(Long id) {
        if (id == null) {
            return null;
        }
        Patient patient = new Patient();
        patient.setId(id);

        return patient;
    }
}
