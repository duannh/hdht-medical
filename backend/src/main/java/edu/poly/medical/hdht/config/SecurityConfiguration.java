package edu.poly.medical.hdht.config;

import edu.poly.medical.hdht.config.jwt.JwtFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;

import static edu.poly.medical.hdht.common.AuthoritiesConstants.*;
import static edu.poly.medical.hdht.common.ResourcesConstants.*;

/**
 * The type Security configuration.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private DomainUserDetailsService userDetailsService;

    /**
     * Jwt filter jwt filter.
     *
     * @return the jwt filter
     */
    @Bean
    public JwtFilter jwtFilter() {
        return new JwtFilter();
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        // Get AuthenticationManager bean
        return super.authenticationManagerBean();
    }

    /**
     * Password encoder password encoder.
     *
     * @return the password encoder
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Configure global.
     *
     * @param auth the auth
     * @throws Exception the exception
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowUrlEncodedSlash(true);
        return firewall;
    }

    /**
     * .cors() avoid request from another domain
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().disable()
                .csrf().disable()
                .addFilterBefore(jwtFilter(), UsernamePasswordAuthenticationFilter.class)
                .headers().frameOptions().disable()
                .and()
                .authorizeRequests()

                .antMatchers("/contacts/**", "/", "/blog/**", "/about/**", "/home/**", "/doctors/**", "/services/**",
                        "/favicon.ico", "/api/doctors/**", "/api/blog/**","/api/addAppoiment/**","/api/departments/**","/account/**").permitAll()

                .antMatchers(SWAGGER_UI_MAPPING,
                        SWAGGER_UI_MAPPING + RESOURCE_SUFFIX,
                        SWAGGER_RESOURCES_MAPPING + RESOURCE_SUFFIX,
                        API_DOCS_MAPPING,
                        API_DOCS_MAPPING + RESOURCE_SUFFIX,
                        WEBJARS_MAPPING + RESOURCE_SUFFIX).permitAll()
                .antMatchers(SWAGGER_UI_MAPPING, SWAGGER_UI_MAPPING + RESOURCE_SUFFIX,
                        SWAGGER_RESOURCES_MAPPING + RESOURCE_SUFFIX, API_DOCS_MAPPING,
                        API_DOCS_MAPPING + RESOURCE_SUFFIX, WEBJARS_MAPPING + RESOURCE_SUFFIX).permitAll()
                .antMatchers(RESOURCE_API + AUTHENTICATE_MAPPING).permitAll()
                .antMatchers(RESOURCE_API + DEPARTMENT_MAPPING,
                        RESOURCE_API + DEPARTMENT_MAPPING + RESOURCE_SUFFIX).permitAll()
                .antMatchers(RESOURCE_API + APPOINTMENT_MAPPING).hasAuthority(AUTHORITY_USER)
                .antMatchers(RESOURCE_API + APPOINTMENT_REQUEST_MAPPING).hasAnyAuthority(AUTHORITY_NURSE, AUTHORITY_ADMIN)
                .antMatchers(RESOURCE_API + APPOINTMENT_DEPARTMENT_MAPPING).hasAnyAuthority(AUTHORITY_NURSE, AUTHORITY_ADMIN)
                .antMatchers(RESOURCE_API + APPOINTMENT_MAPPING + RESOURCE_SUFFIX).hasAuthority(AUTHORITY_ADMIN)
                .antMatchers(RESOURCE_API + ACCOUNT_MAPPING,
                        RESOURCE_API + ACCOUNT_MAPPING + RESOURCE_SUFFIX).hasAuthority(AUTHORITY_USER)
                .antMatchers(RESOURCE_API + DOCTOR_MAPPING).hasAuthority(AUTHORITY_ADMIN)
                .antMatchers(RESOURCE_API + MEDICAL_HISTORY_MAPPING).hasAuthority(AUTHORITY_USER)
                .antMatchers(RESOURCE_API + NURSE_MAPPING).hasAuthority(AUTHORITY_ADMIN)
                .antMatchers(RESOURCE_API + PATIENT_MAPPING, RESOURCE_API + PATIENT_MAPPING + RESOURCE_SUFFIX).hasAnyAuthority(AUTHORITY_NURSE, AUTHORITY_ADMIN)
                .antMatchers(RESOURCE_API + POSITION_MAPPING).hasAuthority(AUTHORITY_ADMIN)
                .antMatchers(RESOURCE_API + LEVEL_MAPPING).hasAuthority(AUTHORITY_ADMIN)
                .antMatchers(RESOURCE_API + SERVICE_MEDICAL_MAPPING).hasAuthority(AUTHORITY_ADMIN)
                .antMatchers(RESOURCE_API + USER_MAPPING).hasAuthority(AUTHORITY_ADMIN)
                .antMatchers(RESOURCE_API + RESOURCE_SUFFIX).hasAuthority(AUTHORITY_USER)
                .antMatchers(RESOURCE_API + "/upload-avatar").hasAuthority(AUTHORITY_USER)
                .antMatchers("/uploads/**").permitAll()
                .antMatchers("/uploads/", "/uploads").permitAll()
                .antMatchers(RESOURCE_SUFFIX).authenticated()
                .anyRequest().authenticated();
    }

    /**
     * Configure web security for resources static
     *
     * @param web WebSecurity
     */
    @Override
    public void configure(WebSecurity web) {
        web.httpFirewall(allowUrlEncodedSlashHttpFirewall());
        web.ignoring().antMatchers("/css" + RESOURCE_SUFFIX,
                "/js" + RESOURCE_SUFFIX,
                "/images" + RESOURCE_SUFFIX,
                WEBJARS_MAPPING + RESOURCE_SUFFIX,
                "/fonts" + RESOURCE_SUFFIX)
                .antMatchers(HttpMethod.OPTIONS, RESOURCE_SUFFIX)
                .antMatchers("/uploads/**", "/uploads/", "/uploads")
                .antMatchers("/swagger-ui/index.html")
                .antMatchers("/test" + RESOURCE_SUFFIX);
    }
}