package edu.poly.medical.hdht.common;

/**
 * The type Constants.
 *
 * @author Huu Duan
 */
public class Constants {

    /**
     * The constant APPOINTMENT_REQUEST.
     */
    public static final Integer APPOINTMENT_REQUEST = 1;

}