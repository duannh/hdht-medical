package edu.poly.medical.hdht.web.controller;

import edu.poly.medical.hdht.service.DepartmentService;
import edu.poly.medical.hdht.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * The type Service controller.
 */
@Controller
@RequestMapping("/services")
public class ServiceController {
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private DoctorService doctorService;
    /**
     * Go to service page string.
     *
     * @return the string
     */
    @GetMapping(value = {"/", ""})
    public String showDepartment(Model model) {
        model.addAttribute("department",departmentService.findAll());
        return "services";
    }
    @GetMapping(value = "showdepartbydoctor/{id}")
    public String findDoctorByDepart(Model model, @PathVariable Long id){
    model.addAttribute("doctorByDepartment", doctorService.findADoctorByDepart(id));

        return "FacultyInformation";
    }

}
