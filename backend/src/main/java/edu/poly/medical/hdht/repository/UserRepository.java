package edu.poly.medical.hdht.repository;

import edu.poly.medical.hdht.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface User repository.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Find by phone user.
     *
     * @param phone the phone
     * @return the user
     */
    User findByPhone(String phone);

    /**
     * Exists by phone boolean.
     *
     * @param phone the phone
     * @return the boolean
     */
    boolean existsByPhone(String phone);

}
