package edu.poly.medical.hdht.service.mapper;

import edu.poly.medical.hdht.domain.Doctor;
import edu.poly.medical.hdht.service.dto.DoctorDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The type Doctor mapper.
 */
@Component
public class DoctorMapper implements EntityMapper<Doctor, DoctorDTO> {

    @Autowired
    private AppointmentMapper appointmentMapper;

    @Autowired
    private DepartmentMapper departmentMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public Doctor toEntity(DoctorDTO dto) {
        Doctor entity = new Doctor();
        entity.setId(dto.getId());
        entity.setBonus(dto.getBonus());
        entity.setExperience(dto.getExperience());
        entity.setFunction(dto.getFunction());
        entity.setIntro(dto.getIntro());
        entity.setStudying(dto.getStudying());
//        if (Optional.ofNullable(dto.getAppointments()).isPresent()) {
//            entity.setAppointments(appointmentMapper.toEntities(dto.getAppointments()));
//        }
        entity.setDepartment(departmentMapper.toEntityFromId(dto.getDepartmentId()));
        entity.setStatus(dto.getStatus());
        entity.setUser(userMapper.toEntityFromId(dto.getUserId()));

        return entity;
    }

    @Override
    public DoctorDTO toDto(Doctor entity) {
        DoctorDTO dto = new DoctorDTO();
        dto.setId(entity.getId());
//        if (Optional.ofNullable(entity.getAppointments()).isPresent()) {
//            dto.setAppointments(appointmentMapper.toDTOs(entity.getAppointments()));
//        }
        dto.setDepartmentId(entity.getDepartment().getId());
        dto.setDepartmentName(entity.getDepartment().getName());
        dto.setStatus(entity.getStatus());
        dto.setUserId(entity.getUser().getId());
        dto.setBonus(entity.getBonus());
        dto.setExperience(entity.getExperience());
        dto.setFunction(entity.getFunction());
        dto.setIntro(entity.getIntro());
        dto.setStudying(dto.getStudying());

        dto.setFirstName(entity.getUser().getFirstName());
        dto.setLastName(entity.getUser().getLastName());
        dto.setPhone(entity.getUser().getPhone());
        dto.setGender(entity.getUser().getGender());
        dto.setImageUrl(entity.getUser().getImageUrl());
        dto.setEmail(entity.getUser().getEmail());
        dto.setBirthday(entity.getUser().getBirthday());
        dto.setAddress(entity.getUser().getAddress());
        dto.setActivated(entity.getUser().getActivated());
        dto.setCreatedDate(entity.getUser().getCreatedDate());
        dto.setAuthorities(entity.getUser().getAuthorities());
        dto.setPassword(entity.getUser().getPassword());

        return dto;
    }

    @Override
    public List<DoctorDTO> toDTOs(List<Doctor> entities) {
        return entities.stream()
                .filter(Objects::nonNull)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Doctor> toEntities(List<DoctorDTO> dtos) {
        return dtos.stream()
                .filter(Objects::nonNull)
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

    @Override
    public Doctor toEntityFromId(Long id) {
        if (id == null) {
            return null;
        }
        Doctor doctor = new Doctor();
        doctor.setId(id);

        return doctor;
    }

}