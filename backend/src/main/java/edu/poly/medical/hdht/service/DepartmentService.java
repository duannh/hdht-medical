package edu.poly.medical.hdht.service;

import edu.poly.medical.hdht.service.dto.DepartmentDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * The interface Department service.
 */
public interface DepartmentService {

    /**
     * Delete by id.
     *
     * @param id the id
     */
    void deleteById(Long id);

    /**
     * Exists by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    boolean existsById(Long id);

    /**
     * Find by id optional.
     *
     * @param id the id
     * @return the optional
     */
    Optional<DepartmentDTO> findById(Long id);

    /**
     * Find all page.
     *
     * @param pageable the pageable
     * @return the page
     */
    Page<DepartmentDTO> findAll(Pageable pageable);

    /**
     * Find all page.
     *
     * @return the list
     */
    List<DepartmentDTO> findAll();

    /**
     * Save department dto.
     *
     * @param dto the dto
     * @return the department dto
     */
    DepartmentDTO save(DepartmentDTO dto);

    /**
     * Find by name department dto.
     *
     * @param name the name
     * @return the department dto
     */
    DepartmentDTO findByName(String name);

    /**
     * Exists by name boolean.
     *
     * @param name the name
     * @return the boolean
     */
    Boolean existsByName(String name);

}
