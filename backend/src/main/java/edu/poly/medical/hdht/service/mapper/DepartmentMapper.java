package edu.poly.medical.hdht.service.mapper;

import edu.poly.medical.hdht.domain.Department;
import edu.poly.medical.hdht.service.dto.DepartmentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The type Department mapper.
 */
@Component
public class DepartmentMapper implements EntityMapper<Department, DepartmentDTO> {

    @Autowired
    private AppointmentMapper appointmentMapper;

    @Autowired
    private DoctorMapper doctorMapper;

    @Autowired
    private NurseMapper nurseMapper;

    @Override
    public Department toEntity(DepartmentDTO dto) {
        Department entity = new Department();
        if (Optional.ofNullable(dto.getAppointments()).isPresent()) {
            entity.setAppointments(appointmentMapper.toEntities(dto.getAppointments()));
        }
        if (Optional.ofNullable(dto.getDoctors()).isPresent()) {
            entity.setDoctors(doctorMapper.toEntities(dto.getDoctors()));
        }
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setNote(dto.getNote());
        entity.setIntroduction(dto.getIntroduction());
        if (Optional.ofNullable(dto.getNurses()).isPresent()) {
            entity.setNurses(nurseMapper.toEntities(dto.getNurses()));
        }

        return entity;
    }

    @Override
    public DepartmentDTO toDto(Department entity) {
        DepartmentDTO dto = new DepartmentDTO();
        dto.setId(entity.getId());
        if (Optional.ofNullable(entity.getDoctors()).isPresent()) {
            dto.setDoctors(doctorMapper.toDTOs(entity.getDoctors()));
        }
        if (Optional.ofNullable(entity.getNurses()).isPresent()) {
            dto.setNurses(nurseMapper.toDTOs(entity.getNurses()));
        }
        if (Optional.ofNullable(entity.getAppointments()).isPresent()) {
            dto.setAppointments(appointmentMapper.toDTOs(entity.getAppointments()));
        }
        dto.setName(entity.getName());
        dto.setNote(entity.getNote());
        dto.setIntroduction(entity.getIntroduction());

        return dto;
    }

    @Override
    public List<DepartmentDTO> toDTOs(List<Department> entities) {
        return entities.stream()
                .filter(Objects::nonNull)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Department> toEntities(List<DepartmentDTO> dtos) {
        return dtos.stream()
                .filter(Objects::nonNull)
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

    @Override
    public Department toEntityFromId(Long id) {
        if (id == null) {
            return null;
        }
        Department department = new Department();
        department.setId(id);

        return department;
    }

}
