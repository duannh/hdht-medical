package edu.poly.medical.hdht.repository;

import edu.poly.medical.hdht.domain.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface Department repository.
 */
@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {

    /**
     * Exists by name boolean.
     *
     * @param name the name
     * @return the boolean
     */
    Boolean existsByName(String name);

    /**
     * Find by name department.
     *
     * @param name the name
     * @return the department
     */
    Department findByName(String name);

}