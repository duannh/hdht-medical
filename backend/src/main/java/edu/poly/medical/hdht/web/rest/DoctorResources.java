package edu.poly.medical.hdht.web.rest;

import edu.poly.medical.hdht.common.StringUtils;
import edu.poly.medical.hdht.service.DoctorService;
import edu.poly.medical.hdht.service.EmailService;
import edu.poly.medical.hdht.service.UserService;
import edu.poly.medical.hdht.service.dto.DoctorDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static edu.poly.medical.hdht.common.ResourcesConstants.*;

/**
 * The type Doctor resources.
 *
 * @author Huu Duan
 */
@RestController
@RequestMapping(RESOURCE_API)
public class DoctorResources {

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private JavaMailSender mailSender;

    /**
     * Gets all doctors.
     *
     * @return the all doctors
     */
    @GetMapping(DOCTOR_MAPPING)
    public ResponseEntity<List<DoctorDTO>> getAllDoctors() {
        List<DoctorDTO> doctors = doctorService.findAll();
        return ResponseEntity.ok(doctors);
    }

    /**
     * Create doctor response entity.
     *
     * @param dto the dto
     * @return the response entity
     */
    @PostMapping(DOCTOR_MAPPING)
    public ResponseEntity<DoctorDTO> createDoctor(@Valid @RequestBody DoctorDTO dto) {
        if (dto.getId() != null) {
            return ResponseEntity.badRequest().build();
        }

        if (userService.existsByPhone(dto.getPhone())) {
            return ResponseEntity.badRequest().build();
        }

        String password = StringUtils.generatePassayPassword();
        dto.setPassword(passwordEncoder.encode(password));

//        String body = TwilioSms.createAccount(password);
        try {
//            TwilioSms.sendMessage(dto.getPhone(), body);
//            ManageSms.sendSms(dto.getPhone(), body);

            String token = UUID.randomUUID().toString();
            MimeMessage mimeMessage = emailService.constructResetTokenEmail("",
                    token, password, dto.getEmail(), dto.getFirstName(), dto.getLastName());
            mailSender.send(mimeMessage);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }

        DoctorDTO doctorDTO = doctorService.save(dto);

        return Optional.ofNullable(doctorDTO)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Update doctor response entity.
     *
     * @param dto the dto
     * @return the response entity
     */
    @PutMapping(DOCTOR_MAPPING)
    public ResponseEntity<DoctorDTO> updateDoctor(@Valid @RequestBody DoctorDTO dto) {
        if (dto.getId() == null) {
            return ResponseEntity.badRequest().build();
        }
        return Optional.ofNullable(doctorService.save(dto))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Gets doctor by id.
     *
     * @param id the id
     * @return the doctor by id
     */
    @GetMapping(DOCTOR_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<DoctorDTO> getDoctorById(@PathVariable Long id) {
        return doctorService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Delete doctor by id response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping(DOCTOR_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<Void> deleteDoctorById(@PathVariable Long id) {
        doctorService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping(DOCTOR_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<List<DoctorDTO>> findDoctorByDepart(@PathVariable Long id) {
        List<DoctorDTO> doctors = doctorService.findADoctorByDepart(id);
        if (doctors.isEmpty()){
            return ResponseEntity.ok(doctorService.findAll());
        }

        return ResponseEntity.ok(doctors);
    }
}