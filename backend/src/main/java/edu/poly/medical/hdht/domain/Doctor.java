package edu.poly.medical.hdht.domain;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The type Doctor.
 */
@Data
@Entity
@Table(name = "hdht_doctor")
public class Doctor implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /*trạng thái*/
    @Column(name = "status")
    private Integer status;

    @OneToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    /**
     * qua trinh hoc tap
     */
    @Column(name = "studying")
    private String studying;

    /**
     * kinh nghiem lam viec
     */
    @Column(name = "exp")
    private String experience;

    /**
     * gioi thieu
     */
    @Column(name = "intro")
    private String intro;

    @Column(name = "bonus")
    private String bonus;

    /**
     * chuc vu
     */
    @Column(name = "function_user")
    private String function;

    @ManyToOne(targetEntity = Department.class)
    @JoinColumn(nullable = false, name = "department_id")
    private Department department;

    @OneToMany(mappedBy = "doctor", cascade = CascadeType.REMOVE)
    private List<Appointment> appointments;

}
