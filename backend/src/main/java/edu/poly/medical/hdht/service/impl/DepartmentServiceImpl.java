package edu.poly.medical.hdht.service.impl;

import edu.poly.medical.hdht.domain.Department;
import edu.poly.medical.hdht.repository.DepartmentRepository;
import edu.poly.medical.hdht.service.DepartmentService;
import edu.poly.medical.hdht.service.dto.DepartmentDTO;
import edu.poly.medical.hdht.service.mapper.DepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The type Department service.
 */
@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentMapper departmentMapper;

    @Autowired
    private DepartmentRepository departmentRepository;

    /**
     * @param name
     * @return
     * @see edu.poly.medical.hdht.repository.DepartmentRepository#existsByName(java.lang.String)
     */
    @Override
    public Boolean existsByName(String name) {
        return departmentRepository.existsByName(name);
    }

    /**
     * @param name
     * @return
     * @see edu.poly.medical.hdht.repository.DepartmentRepository#findByName(java.lang.String)
     */
    @Override
    public DepartmentDTO findByName(String name) {
        return Optional.ofNullable(departmentRepository.findByName(name)).map(departmentMapper::toDto).orElse(null);
    }

    /**
     * @param dto
     * @return
     * @see org.springframework.data.repository.CrudRepository#save(java.lang.Object)
     */
    @Override
    public DepartmentDTO save(DepartmentDTO dto) {
        Department department = departmentMapper.toEntity(dto);

        return Optional.ofNullable(departmentRepository.save(department)).map(departmentMapper::toDto).orElse(null);
    }

    /**
     * @param pageable
     * @return
     * @see org.springframework.data.repository.PagingAndSortingRepository#findAll(org.springframework.data.domain.Pageable)
     */
    @Override
    public Page<DepartmentDTO> findAll(Pageable pageable) {
        return departmentRepository.findAll(pageable).map(departmentMapper::toDto);
    }

    @Override
    public List<DepartmentDTO> findAll() {
        return departmentRepository.findAll().stream()
                .map(departmentMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * @param id
     * @return
     * @see org.springframework.data.repository.CrudRepository#findById(java.lang.Object)
     */
    @Override
    public Optional<DepartmentDTO> findById(Long id) {
        return departmentRepository.findById(id).map(departmentMapper::toDto);
    }

    /**
     * @param id
     * @return
     * @see org.springframework.data.repository.CrudRepository#existsById(java.lang.Object)
     */
    @Override
    public boolean existsById(Long id) {
        return departmentRepository.existsById(id);
    }

    /**
     * @param id
     * @see org.springframework.data.repository.CrudRepository#deleteById(java.lang.Object)
     */
    @Override
    public void deleteById(Long id) {
        departmentRepository.deleteById(id);
    }

}
