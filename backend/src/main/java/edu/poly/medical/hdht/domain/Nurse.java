package edu.poly.medical.hdht.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The type Nurse.
 */
@Data
@Entity
@Table(name = "hdht_nurse")
public class Nurse implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * qua trinh hoc tap
     */
    @Column(name = "studying")
    private String studying;

    /**
     * kinh nghiem lam viec
     */
    @Column(name = "exp")
    private String experience;

    /**
     * gioi thieu
     */
    @Column(name = "intro")
    private String intro;

    @Column(name = "function_user")
    private String function;

    @OneToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    @ManyToOne(targetEntity = Department.class)
    @JoinColumn(nullable = false, name = "department_id")
    private Department department;

//    @OneToMany(mappedBy = "nurse", cascade = CascadeType.REMOVE)
//    private List<Appointment> appointments;

}
