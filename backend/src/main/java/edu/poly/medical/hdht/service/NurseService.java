package edu.poly.medical.hdht.service;

import edu.poly.medical.hdht.service.dto.NurseDTO;
import edu.poly.medical.hdht.service.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * The interface Nurse service.
 */
public interface NurseService {

    /**
     * Find all page.
     *
     * @return the page
     */
    List<NurseDTO> findAll();

    /**
     * Save nurse dto.
     *
     * @param dto the dto
     * @return the nurse dto
     */
    NurseDTO save(NurseDTO dto);

    /**
     * Find by id optional.
     *
     * @param aLong the a long
     * @return the optional
     */
    Optional<NurseDTO> findById(Long aLong);

    /**
     * Exists by id boolean.
     *
     * @param aLong the a long
     * @return the boolean
     */
    boolean existsById(Long aLong);

    /**
     * Delete by id.
     *
     * @param aLong the a long
     */
    void deleteById(Long aLong);

    /**
     * Find by user optional.
     *
     * @param user the user
     * @return the optional
     */
    Optional<NurseDTO> findByUser(UserDTO user);

}
