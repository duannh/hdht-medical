package edu.poly.medical.hdht.service.mapper;

import edu.poly.medical.hdht.domain.MedicalHistory;
import edu.poly.medical.hdht.service.dto.MedicalHistoryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * The type Medical history mapper.
 */
@Component
public class MedicalHistoryMapper implements EntityMapper<MedicalHistory, MedicalHistoryDTO> {

    @Autowired
    private AppointmentMapper appointmentMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public MedicalHistory toEntity(MedicalHistoryDTO dto) {
        MedicalHistory medicalHistory = new MedicalHistory();
        medicalHistory.setId(dto.getId());
        medicalHistory.setAdviceFromDoctor(dto.getAdviceFromDoctor());
        medicalHistory.setAppointment(appointmentMapper.toEntityFromId(dto.getAppointmentId()));
        medicalHistory.setInformation(dto.getInformation());
        medicalHistory.setNote(dto.getNote());
        medicalHistory.setRemedial(dto.getRemedial());
        medicalHistory.setUser(userMapper.toEntityFromId(dto.getUserId()));
        medicalHistory.setBloodGroup(dto.getBloodGroup());
        medicalHistory.setBloodPressure(dto.getBloodPressure());
        medicalHistory.setSignsOfIllness(dto.getSignsOfIllness());
        medicalHistory.setWeight(dto.getWeight());
        medicalHistory.setTempBody(dto.getTempBody());
        medicalHistory.setHeight(dto.getHeight());

        return medicalHistory;
    }

    @Override
    public MedicalHistoryDTO toDto(MedicalHistory entity) {
        MedicalHistoryDTO medicalHistoryDTO = new MedicalHistoryDTO();
        medicalHistoryDTO.setAdviceFromDoctor(entity.getAdviceFromDoctor());
        medicalHistoryDTO.setId(entity.getId());
        medicalHistoryDTO.setInformation(entity.getInformation());
        medicalHistoryDTO.setNote(entity.getNote());
        medicalHistoryDTO.setRemedial(entity.getRemedial());
        medicalHistoryDTO.setUserId(entity.getUser().getId());
        medicalHistoryDTO.setAppointmentId(entity.getAppointment().getId());
        medicalHistoryDTO.setBloodGroup(entity.getBloodGroup());
        medicalHistoryDTO.setBloodPressure(entity.getBloodPressure());
        medicalHistoryDTO.setSignsOfIllness(entity.getSignsOfIllness());
        medicalHistoryDTO.setWeight(entity.getWeight());
        medicalHistoryDTO.setTempBody(entity.getTempBody());
        medicalHistoryDTO.setHeight(entity.getHeight());

        return medicalHistoryDTO;
    }

    @Override
    public List<MedicalHistoryDTO> toDTOs(List<MedicalHistory> entities) {
        return entities.stream()
                .filter(Objects::nonNull)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<MedicalHistory> toEntities(List<MedicalHistoryDTO> dtos) {
        return dtos.stream()
                .filter(Objects::nonNull)
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

    @Override
    public MedicalHistory toEntityFromId(Long id) {
        if (id == null) {
            return null;
        }
        MedicalHistory medicalHistory = new MedicalHistory();
        medicalHistory.setId(id);

        return medicalHistory;
    }
}
