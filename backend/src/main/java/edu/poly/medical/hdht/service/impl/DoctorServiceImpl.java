package edu.poly.medical.hdht.service.impl;

import edu.poly.medical.hdht.domain.Doctor;
import edu.poly.medical.hdht.domain.User;
import edu.poly.medical.hdht.repository.DoctorRepository;
import edu.poly.medical.hdht.repository.UserRepository;
import edu.poly.medical.hdht.service.DoctorService;
import edu.poly.medical.hdht.service.UserService;
import edu.poly.medical.hdht.service.dto.DoctorDTO;
import edu.poly.medical.hdht.service.dto.UserDTO;
import edu.poly.medical.hdht.service.mapper.DoctorMapper;
import edu.poly.medical.hdht.service.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The type Doctor service.
 */
@Service
@Slf4j
public class DoctorServiceImpl implements DoctorService {

    @Autowired
    private DoctorMapper doctorMapper;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;

    /**
     * Returns a {@link Page} of entities meeting the paging restriction provided in the {@code Pageable} object.
     *
     * @param pageable
     * @return a page of entities
     */
    @Override
    public Page<DoctorDTO> findAll(Pageable pageable) {
        return doctorRepository.findAll(pageable).map(doctorMapper::toDto);
    }

    @Override
    public List<DoctorDTO> findAll() {
        return doctorRepository.findAll().stream()
                .map(doctorMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed the
     * entity instance completely.
     *
     * @param dto must not be {@literal null}.
     * @return the saved entity; will never be {@literal null}.
     */
    @Override
    public DoctorDTO save(DoctorDTO dto) {
        User user = userService.save(dto);
        Doctor entity = doctorMapper.toEntity(dto);
        entity.setUser(user);

        return Optional.ofNullable(doctorRepository.save(entity))
                .map(doctorMapper::toDto)
                .orElse(null);
    }

    /**
     * Retrieves an entity by its id.
     *
     * @param aLong must not be {@literal null}.
     * @return the entity with the given id or {@literal Optional#empty()} if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    public Optional<DoctorDTO> findById(Long aLong) {
        return doctorRepository.findById(aLong).map(doctorMapper::toDto);
    }

    /**
     * Returns whether an entity with the given id exists.
     *
     * @param aLong must not be {@literal null}.
     * @return {@literal true} if an entity with the given id exists, {@literal false} otherwise.
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    public boolean existsById(Long aLong) {
        return doctorRepository.existsById(aLong);
    }

    /**
     * Deletes the entity with the given id.
     *
     * @param aLong must not be {@literal null}.
     * @throws IllegalArgumentException in case the given {@code id} is {@literal null}
     */
    @Override
    public void deleteById(Long aLong) {
        doctorRepository.deleteById(aLong);
    }

    /**
     * Retrieves an entity by its user.
     *
     * @param userDTO must not be {@literal null}.
     * @return the entity with the given id or {@literal Optional#empty()} if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    public Optional<DoctorDTO> findByUser(UserDTO userDTO) {
        User user = userMapper.toEntity(userDTO);
        return doctorRepository.findByUser(user)
                .map(doctorMapper::toDto);
    }

    @Override
    public List<DoctorDTO> findADoctorByDepart(Long id) {
        return doctorRepository.findADoctorByDepart(id).stream()
                .map(doctorMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<DoctorDTO> findSearchName(String name_doctor) {
        return doctorRepository.findSearchName(name_doctor).stream()
                .map(doctorMapper::toDto)
                .collect(Collectors.toList());
    }
}
