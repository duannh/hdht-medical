package edu.poly.medical.hdht.service;

import edu.poly.medical.hdht.service.dto.*;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * The interface Appointment service.
 */
public interface AppointmentService {

    /**
     * Delete by id.
     *
     * @param id the id
     */
    void deleteById(Long id);

    /**
     * Exists by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    boolean existsById(Long id);

    /**
     * Find by id optional.
     *
     * @param id the id
     * @return the optional
     */
    Optional<AppointmentDTO> findById(Long id);

    /**
     * Find all page.
     *
     * @return the page
     */
    List<AppointmentDTO> findAll();

    /**
     * Save s.
     *
     * @param <S> the type parameter
     * @param dto the dto
     * @return the s
     */
    <S extends AppointmentDTO> S save(S dto);

    /**
     * Find all by status is request page.
     *
     * @return the page
     */
    List<AppointmentDTO> findAllByStatusIsRequest();

    /**
     * Find all by status page.
     *
     * @param status the status
     * @return the page
     */
    List<AppointmentDTO> findAllByStatus(Integer status);

    /**
     * Find all by nurse page.
     *
     * @param userDTO the user dto
     * @return the page
     */
    List<AppointmentDTO> findAllByCreatedBy(UserDTO userDTO);

    /**
     * Find all by doctor page.
     *
     * @param doctorDTO the doctor dto
     * @return the page
     */
    List<AppointmentDTO> findAllByDoctor(DoctorDTO doctorDTO);

    /**
     * Find all by patient page.
     *
     * @param patientDTO the patient dto
     * @return the page
     */
    List<AppointmentDTO> findAllByPatient(PatientDTO patientDTO);

    /**
     * Find all by start time between page.
     *
     * @param firstTime the first time
     * @param lastTime  the last time
     * @return the page
     */
    List<AppointmentDTO> findAllByStartTimeBetween(Instant firstTime, Instant lastTime);

    /**
     * Find all by finish time between page.
     *
     * @param firstTime the first time
     * @param lastTime  the last time
     * @return the page
     */
    List<AppointmentDTO> findAllByFinishTimeBetween(Instant firstTime, Instant lastTime);

    /**
     * Find all by department page.
     *
     * @param <S>      the type parameter
     * @param nurseDTO the nurse dto
     * @return the page
     */
    <S extends AppointmentDTO> List<S> findAllByDepartment(NurseDTO nurseDTO);

}
