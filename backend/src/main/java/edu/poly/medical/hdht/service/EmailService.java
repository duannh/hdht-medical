package edu.poly.medical.hdht.service;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

import static edu.poly.medical.hdht.common.StringUtils.ENCODING_UTF_8;

@Service
public class EmailService {

    private final TemplateEngine templateEngine;

    private final JavaMailSender mailSender;

    public EmailService(JavaMailSender mailSender, TemplateEngine templateEngine) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
    }

    /**
     * Get URL of application use StringBuilder
     *
     * @param request : the HttpServletRequest
     * @return url the url of application
     */
    public StringBuilder getAppUrl(HttpServletRequest request) {
        StringBuilder urlAppBuilder = new StringBuilder();
        urlAppBuilder.append("http://");
        urlAppBuilder.append(request.getServerName());
        urlAppBuilder.append(":");
        urlAppBuilder.append(request.getServerPort());
        urlAppBuilder.append(request.getContextPath());

        return urlAppBuilder;
    }

    public MimeMessage constructResetTokenEmail(
            String contextPath, String token, String password, String email, String firstName, String lastName) throws MessagingException {
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(contextPath);
//        urlBuilder.append("/change-password?id=");
        urlBuilder.append("/change-password?");
//        urlBuilder.append(user.getId());
//        urlBuilder.append("&token=");
        urlBuilder.append("token=");
        urlBuilder.append(token);

        return constructEmail("Welcome to HDHT - Medical", urlBuilder.toString(), password, email, firstName, lastName);
    }

    private MimeMessage constructEmail(String subject, String body,
                                       String password, String email
            , String firstName, String lastName) throws MessagingException {
        // Prepare the evaluation context
        Context ctx = new Context(LocaleContextHolder.getLocale());
        StringBuilder nameBuilder = new StringBuilder();
        nameBuilder.append(firstName);
        nameBuilder.append(" ");
        nameBuilder.append(lastName);

        ctx.setVariable("name", nameBuilder);
        ctx.setVariable("subscriptionDate", new Date());
        ctx.setVariable("url", body);
        ctx.setVariable("password", password);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        MimeMessageHelper message =
                new MimeMessageHelper(mimeMessage, false, ENCODING_UTF_8);

        // Create the HTML body using Thymeleaf
        String htmlContent = this.templateEngine.process("mail/emailTemplate.html", ctx);

        // true = isHtml
        message.setText(htmlContent, true);
        message.setSubject(subject);
        message.setTo(email);

        return mimeMessage;
    }

}
