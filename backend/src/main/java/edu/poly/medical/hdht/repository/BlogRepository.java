package edu.poly.medical.hdht.repository;

import edu.poly.medical.hdht.domain.Blog;
import edu.poly.medical.hdht.service.dto.BlogDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BlogRepository extends JpaRepository<Blog, Long> {
    @Query(value = "select  * from hdht_blog where  id = ?", nativeQuery = true)
    BlogDTO findBlogById(Long id);
}
