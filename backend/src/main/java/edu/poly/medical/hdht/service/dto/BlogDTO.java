package edu.poly.medical.hdht.service.dto;

import lombok.Data;

import java.util.Date;
@Data
public class BlogDTO {
    private Long id;
    private String name;
    private String text;
    private String image;
    private Date createDate;
}
