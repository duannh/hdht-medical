package edu.poly.medical.hdht.web.controller;

import edu.poly.medical.hdht.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The type Blog controller.
 */
@Controller
@RequestMapping("/blog")
public class BlogController {

    @Autowired
    private BlogService blogService;
    /**
     * Go to service page string.
     *
     * @return the string
     */
    @GetMapping(value = {"/", ""})
    public String goToServicePage(Model model) {
        model.addAttribute("blog", blogService.findAll());
        return "blog";
    }

    /**
     * Go to service page sigle string.
     *
     * @return the string
     */
    @GetMapping("/single")
    public String goToServicePageSigle() {
        return "blog-single";
    }

}
