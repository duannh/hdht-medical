package edu.poly.medical.hdht.web.rest;

import edu.poly.medical.hdht.common.StringUtils;
import edu.poly.medical.hdht.service.EmailService;
import edu.poly.medical.hdht.service.PatientService;
import edu.poly.medical.hdht.service.UserService;
import edu.poly.medical.hdht.service.dto.PatientDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static edu.poly.medical.hdht.common.ResourcesConstants.*;

/**
 * The type Patient resources.
 *
 * @author Huu Duan
 */
@RestController
@RequestMapping(RESOURCE_API)
public class PatientResources {

    @Autowired
    private PatientService patientService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private JavaMailSender mailSender;

    /**
     * Gets all patients.
     *
     * @param page the page
     * @param size the size
     * @return the all patients
     */
    @GetMapping(PATIENT_MAPPING + RESOURCE_GET_PAGING)
    public ResponseEntity<List<PatientDTO>> getAllPatients(
            @RequestParam("page") int page, @RequestParam("size") int size) {
        Sort sort = new Sort(new Sort.Order(Sort.Direction.ASC, "id"));
        Pageable pageable = new PageRequest(page, size, sort);

        Page<PatientDTO> patients = patientService.findAll(pageable);

        return ResponseEntity.ok(patients.getContent());
    }

    /**
     * Gets all patients.
     *
     * @return the all patients
     */
    @GetMapping(PATIENT_MAPPING)
    public ResponseEntity<List<PatientDTO>> getAllPatients() {
        List<PatientDTO> patients = patientService.findAll();
        return ResponseEntity.ok(patients);
    }

    /**
     * Create patient response entity.
     *
     * @param dto the dto
     * @return the response entity
     */
    @PostMapping(PATIENT_MAPPING)
    public ResponseEntity<PatientDTO> createPatient(@Valid @RequestBody PatientDTO dto) {
        if (dto.getId() != null) {
            return ResponseEntity.badRequest().build();
        }

        if (userService.existsByPhone(dto.getPhone())) {
            return ResponseEntity.badRequest().build();
        }

        String password = StringUtils.generatePassayPassword();
        dto.setPassword(passwordEncoder.encode(password));

//        String body = TwilioSms.createAccount(password);
        try {
//            TwilioSms.sendMessage(dto.getPhone(), body);

            String token = UUID.randomUUID().toString();
            MimeMessage mimeMessage = emailService.constructResetTokenEmail("",
                    token, password, dto.getEmail(), dto.getFirstName(), dto.getLastName());
            mailSender.send(mimeMessage);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }

        PatientDTO patientDTO = patientService.save(dto);

        return Optional.ofNullable(patientDTO)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Update patient response entity.
     *
     * @param dto the dto
     * @return the response entity
     */
    @PutMapping(PATIENT_MAPPING)
    public ResponseEntity<PatientDTO> updatePatient(@Valid @RequestBody PatientDTO dto) {
        if (dto.getId() == null) {
            return ResponseEntity.badRequest().build();
        }

        return Optional.ofNullable(patientService.save(dto))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Gets patient by id.
     *
     * @param id the id
     * @return the patient by id
     */
    @GetMapping(PATIENT_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<PatientDTO> getPatientById(@PathVariable Long id) {
        return patientService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Delete patient by id response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping(PATIENT_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<Void> deletePatientById(@PathVariable Long id) {
        patientService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}