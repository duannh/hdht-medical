package edu.poly.medical.hdht.repository;

import edu.poly.medical.hdht.domain.Patient;
import edu.poly.medical.hdht.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The interface Patient repository.
 */
@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

    /**
     * Find by user optional.
     *
     * @param user the user
     * @return the optional
     */
    Optional<Patient> findByUser(User user);

}
