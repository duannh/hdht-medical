package edu.poly.medical.hdht.service.dto;

import lombok.Data;

import java.util.List;

/**
 * The type Department dto.
 */
@Data
public class DepartmentDTO {

    private Long id;

    private String name;

    private String note;

    private String introduction;

    private List<DoctorDTO> doctors;

    private List<NurseDTO> nurses;

    private List<AppointmentDTO> appointments;

}