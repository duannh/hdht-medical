package edu.poly.medical.hdht.service.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * The type Nurse dto.
 */
@Data
//@EqualsAndHashCode(callSuper = false)
public class NurseDTO extends UserDTO {

    private Long id;

    /**
     * qua trinh hoc tap
     */
    private String studying;

    /**
     * kinh nghiem lam viec
     */
    private String experience;

    /**
     * gioi thieu
     */
    private String intro;

    private Long userId;

    private Long departmentId;

    private String function;

    private String departmentName;

    private List<AppointmentDTO> appointments;

}
