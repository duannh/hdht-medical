package edu.poly.medical.hdht.service.dto;

import lombok.Data;

import java.util.List;

/**
 * The type Doctor dto.
 */
@Data
public class DoctorDTO extends UserDTO {

    private Long id;

    /**
     * qua trinh hoc tap
     */
    private String studying;

    /**
     * kinh nghiem lam viec
     */
    private String experience;

    /**
     * gioi thieu
     */
    private String intro;

    private String bonus;

    /**
     * chuc vu
     */
    private String function;

    private Integer status;

    private Long userId;

    private Long departmentId;

    private String departmentName;

    private List<AppointmentDTO> appointments;

}
