package edu.poly.medical.hdht.service;

import edu.poly.medical.hdht.service.dto.DepartmentDTO;
import edu.poly.medical.hdht.service.dto.MedicalHistoryDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * The interface Medical history service.
 */
public interface MedicalHistoryService {

    /**
     * Find all page.
     *
     * @param pageable the pageable
     * @return the page
     */
    Page<MedicalHistoryDTO> findAll(Pageable pageable);

    /**
     * Find all page.
     *
     * @return the page
     */
    List<MedicalHistoryDTO> findAll();

    /**
     * Save medical history dto.
     *
     * @param dto the dto
     * @return the medical history dto
     */
    MedicalHistoryDTO save(MedicalHistoryDTO dto);

    /**
     * Find by id optional.
     *
     * @param aLong the a long
     * @return the optional
     */
    Optional<MedicalHistoryDTO> findById(Long aLong);

    /**
     * Exists by id boolean.
     *
     * @param aLong the a long
     * @return the boolean
     */
    boolean existsById(Long aLong);

    /**
     * Delete by id.
     *
     * @param aLong the a long
     */
    void deleteById(Long aLong);

}
