package edu.poly.medical.hdht.service.dto;

import lombok.Data;

/**
 * The type Medical history dto.
 */
@Data
public class MedicalHistoryDTO extends AppointmentDTO {

    private Long id;

    private String information;

    private String adviceFromDoctor;

    private String remedial;

    private Integer height;

    private Integer weight;

    private Integer bloodGroup;

    private Integer bloodPressure;

    private Integer tempBody;

    /**
     * dau hieu benh
     */
    private String signsOfIllness;

    private String note;

    private Long appointmentId;

    private Long userId;

}
