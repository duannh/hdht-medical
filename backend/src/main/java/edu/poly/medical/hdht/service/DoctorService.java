package edu.poly.medical.hdht.service;

import edu.poly.medical.hdht.service.dto.DoctorDTO;
import edu.poly.medical.hdht.service.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * The interface Doctor service.
 */
public interface DoctorService {

    /**
     * Find all page.
     *
     * @param pageable the pageable
     * @return the page
     */
    Page<DoctorDTO> findAll(Pageable pageable);

    /**
     * Find all list.
     *
     * @return the list
     */
    List<DoctorDTO> findAll();
    /**
     * Save doctor dto.
     *
     * @param dto the dto
     * @return the doctor dto
     */
    DoctorDTO save(DoctorDTO dto);

    /**
     * Find by id optional.
     *
     * @param aLong the a long
     * @return the optional
     */
    Optional<DoctorDTO> findById(Long aLong);

    /**
     * Exists by id boolean.
     *
     * @param aLong the a long
     * @return the boolean
     */
    boolean existsById(Long aLong);

    /**
     * Delete by id.
     *
     * @param aLong the a long
     */
    void deleteById(Long aLong);

    /**
     * Find by user optional.
     *
     * @param user the user
     * @return the optional
     */
    Optional<DoctorDTO> findByUser(UserDTO user);

    List<DoctorDTO> findADoctorByDepart(Long id);

    List<DoctorDTO> findSearchName(String name_doctor);
}
