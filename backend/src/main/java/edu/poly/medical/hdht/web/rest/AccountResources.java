package edu.poly.medical.hdht.web.rest;

import edu.poly.medical.hdht.common.SecurityUtils;
import edu.poly.medical.hdht.service.DoctorService;
import edu.poly.medical.hdht.service.NurseService;
import edu.poly.medical.hdht.service.PatientService;
import edu.poly.medical.hdht.service.UserService;
import edu.poly.medical.hdht.service.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static edu.poly.medical.hdht.common.ResourcesConstants.ACCOUNT_MAPPING;
import static edu.poly.medical.hdht.common.ResourcesConstants.RESOURCE_API;
import static edu.poly.medical.hdht.common.SecurityUtils.*;

/**
 * The type Account resources.
 */
@RestController
@RequestMapping(RESOURCE_API)
@Slf4j
public class AccountResources {

    @Autowired
    private UserService userService;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private NurseService nurseService;

    @Autowired
    private PatientService patientService;

    /**
     * Gets user logged.
     *
     * @return the user logged
     */
    @GetMapping(ACCOUNT_MAPPING)
    public ResponseEntity<Object> getUserLogged() {
        return ResponseEntity.ok(this.getUserInfoLogged());
    }

    /**
     * Gets current user.
     *
     * @return the current user
     */
    public UserDTO getCurrentUser() {
        Optional<String> phone = SecurityUtils.getCurrentUserLogin();
        return userService.findByPhone(phone.get());
    }

    /**
     * Gets user info logged.
     *
     * @return the user info logged
     */
    public Object getUserInfoLogged() {
//        if (isAdmin()) {
            return userService.findByPhone(this.getCurrentUser().getPhone());
//        } else if (isDoctor()) {
//            return doctorService.findByUser(this.getCurrentUser()).get();
//        } else if (isNurse()) {
//            return nurseService.findByUser(this.getCurrentUser()).get();
//        } else if (isPatient()) {
//        }

//        return patientService.findByUser(this.getCurrentUser()).get();
    }

}
