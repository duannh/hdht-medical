package edu.poly.medical.hdht.service.impl;

import edu.poly.medical.hdht.domain.Patient;
import edu.poly.medical.hdht.domain.User;
import edu.poly.medical.hdht.repository.PatientRepository;
import edu.poly.medical.hdht.service.PatientService;
import edu.poly.medical.hdht.service.UserService;
import edu.poly.medical.hdht.service.dto.PatientDTO;
import edu.poly.medical.hdht.service.dto.UserDTO;
import edu.poly.medical.hdht.service.mapper.PatientMapper;
import edu.poly.medical.hdht.service.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The type Patient service.
 */
@Service
public class PatientServiceImpl implements PatientService {

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserService userService;

    /**
     * Returns a {@link Page} of entities meeting the paging restriction provided in the {@code Pageable} object.
     *
     * @param pageable
     * @return a page of entities
     */
    @Override
    public Page<PatientDTO> findAll(Pageable pageable) {
        return patientRepository.findAll(pageable)
                .map(patientMapper::toDto);
    }

    /**
     * Find all list.
     *
     * @return the list
     */
    @Override
    public List<PatientDTO> findAll() {
        return patientRepository.findAll().stream()
                .map(patientMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed the
     * entity instance completely.
     *
     * @param dto must not be {@literal null}.
     * @return the saved entity; will never be {@literal null}.
     */
    @Override
    public PatientDTO save(PatientDTO dto) {
        User user = userService.save(dto);
        Patient entity = patientMapper.toEntity(dto);
        entity.setUser(user);

        return Optional.ofNullable(patientRepository.save(entity))
                .map(patientMapper::toDto)
                .orElse(null);
    }

    /**
     * Retrieves an entity by its id.
     *
     * @param aLong must not be {@literal null}.
     * @return the entity with the given id or {@literal Optional#empty()} if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    public Optional<PatientDTO> findById(Long aLong) {
        return patientRepository.findById(aLong).map(patientMapper::toDto);
    }

    /**
     * Returns whether an entity with the given id exists.
     *
     * @param aLong must not be {@literal null}.
     * @return {@literal true} if an entity with the given id exists, {@literal false} otherwise.
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    public boolean existsById(Long aLong) {
        return patientRepository.existsById(aLong);
    }

    /**
     * Deletes the entity with the given id.
     *
     * @param aLong must not be {@literal null}.
     * @throws IllegalArgumentException in case the given {@code id} is {@literal null}
     */
    @Override
    public void deleteById(Long aLong) {
        patientRepository.deleteById(aLong);
    }

    /**
     * Retrieves an entity by its user.
     *
     * @param userDTO must not be {@literal null}.
     * @return the entity with the given id or {@literal Optional#empty()} if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}.
     */
    @Override
    public Optional<PatientDTO> findByUser(UserDTO userDTO) {
        User user = userMapper.toEntity(userDTO);
        return patientRepository.findByUser(user)
                .map(patientMapper::toDto);
    }
}
