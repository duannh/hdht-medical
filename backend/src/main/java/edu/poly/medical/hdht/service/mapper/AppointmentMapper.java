package edu.poly.medical.hdht.service.mapper;

import edu.poly.medical.hdht.domain.Appointment;
import edu.poly.medical.hdht.repository.DoctorRepository;
import edu.poly.medical.hdht.repository.NurseRepository;
import edu.poly.medical.hdht.repository.PatientRepository;
import edu.poly.medical.hdht.service.dto.AppointmentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The type Appointment mapper.
 */
@Component
public class AppointmentMapper implements EntityMapper<Appointment, AppointmentDTO> {

    @Autowired
    private DepartmentMapper departmentMapper;

    @Autowired
    private DoctorMapper doctorMapper;

    @Autowired
    private NurseMapper nurseMapper;

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private NurseRepository nurseRepository;

    @Override
    public Appointment toEntity(AppointmentDTO dto) {
        Appointment appointment = new Appointment();
        appointment.setId(dto.getId());
        appointment.setDepartment(departmentMapper.toEntityFromId(dto.getDepartmentId()));
        appointment.setDoctor(dto.getDoctorId() == null ? null : doctorMapper.toEntityFromId(dto.getDoctorId()));
        appointment.setFinishTime(dto.getFinishTime());
        appointment.setLocation(dto.getLocation());
        appointment.setNote(dto.getNote());
        appointment.setCreatedBy(userMapper.toEntityFromId(dto.getCreatedBy()));
        appointment.setPatient(dto.getPatientId() == null ? null : patientMapper.toEntityFromId(dto.getPatientId()));
        appointment.setStartTime(dto.getStartTime());
        appointment.setStatus(dto.getStatus());
        appointment.setShift(dto.getShift());
        appointment.setTimeRegistered(dto.getTimeRegistered());
        appointment.setAppointmentCode(dto.getAppointmentCode());
        appointment.setReason(dto.getReason());
        appointment.setCreatedBy(userMapper.toEntityFromId(dto.getCreatedBy()));

        return appointment;
    }

    @Override
    public AppointmentDTO toDto(Appointment entity) {
        AppointmentDTO dto = new AppointmentDTO();
        dto.setId(entity.getId());
        dto.setDepartmentId(entity.getDepartment().getId());
        dto.setDepartmentName(entity.getDepartment().getName());
        dto.setDoctorDTO(entity.getDoctor() == null ? null : doctorMapper.toDto(doctorRepository.getOne(entity.getDoctor().getId())));
        dto.setDoctorId(entity.getDoctor() == null ? null : doctorMapper.toDto(doctorRepository.getOne(entity.getDoctor().getId())).getId());
        dto.setFinishTime(entity.getFinishTime());
        dto.setLocation(entity.getLocation());
        dto.setNote(entity.getNote());
        dto.setCreatedBy(entity.getCreatedBy() == null ? null : entity.getCreatedBy().getId());
        dto.setPatientDTO(entity.getPatient() == null ? null : patientMapper.toDto(patientRepository.getOne(entity.getPatient().getId())));
        dto.setPatientId(entity.getPatient() == null ? null : patientMapper.toDto(patientRepository.getOne(entity.getPatient().getId())).getId());
        dto.setStartTime(entity.getStartTime());
        dto.setStatus(entity.getStatus());
        dto.setTimeRegistered(entity.getTimeRegistered());
        dto.setShift(entity.getShift());
        dto.setAppointmentCode(entity.getAppointmentCode());
        dto.setReason(entity.getReason());
        dto.setCreatedByUser(userMapper.toDto(entity.getCreatedBy()));

        return dto;
    }

    @Override
    public List<AppointmentDTO> toDTOs(List<Appointment> entities) {
        return entities.stream()
                .filter(Objects::nonNull)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Appointment> toEntities(List<AppointmentDTO> dtos) {
        return dtos.stream()
                .filter(Objects::nonNull)
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

    @Override
    public Appointment toEntityFromId(Long id) {
        if (id == null) {
            return null;
        }
        Appointment appointment = new Appointment();
        appointment.setId(id);

        return appointment;
    }
}
