package edu.poly.medical.hdht.web.controller;

import edu.poly.medical.hdht.service.AppointmentService;
import edu.poly.medical.hdht.service.DoctorService;
import edu.poly.medical.hdht.service.dto.AppointmentDTO;
import edu.poly.medical.hdht.service.dto.PatientDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * The type Client forward controller.
 */
@Controller
public class ClientForwardController {
    @Autowired
    private DoctorService doctorService;
    /**
     * Default mapping string.
     *
     * @return the string
     */
    @GetMapping(value = {"/home", "/", ""})
    public String defaultMapping(Model model) {
        model.addAttribute("doctors", doctorService.findAll());
        return "index";
    }

}
