package edu.poly.medical.hdht.web.rest;

import edu.poly.medical.hdht.common.StringUtils;
import edu.poly.medical.hdht.common.TwilioSms;
import edu.poly.medical.hdht.service.EmailService;
import edu.poly.medical.hdht.service.NurseService;
import edu.poly.medical.hdht.service.UserService;
import edu.poly.medical.hdht.service.dto.NurseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static edu.poly.medical.hdht.common.ResourcesConstants.*;

/**
 * The type Nurse resources.
 */
@RestController
@RequestMapping(RESOURCE_API)
public class NurseResources {

    @Autowired
    private NurseService nurseService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private JavaMailSender mailSender;

    /**
     * Gets all nurses.
     *
     * @return the all nurses
     */
    @GetMapping(NURSE_MAPPING)
    public ResponseEntity<List<NurseDTO>> getAllDoctors() {
        List<NurseDTO> nurses = nurseService.findAll();
        return ResponseEntity.ok(nurses);
    }

    /**
     * Create nurse response entity.
     *
     * @param dto the dto
     * @return the response entity
     */
    @PostMapping(NURSE_MAPPING)
    public ResponseEntity<NurseDTO> createNurse(@RequestBody NurseDTO dto) {
        if (dto.getId() != null) {
            return ResponseEntity.badRequest().build();
        }

        if (userService.existsByPhone(dto.getPhone())) {
            return ResponseEntity.badRequest().build();
        }

        String password = StringUtils.generatePassayPassword();
        dto.setPassword(passwordEncoder.encode(password));

//        String body = TwilioSms.createAccount(password);
        try {
//            TwilioSms.sendMessage(dto.getPhone(), body);

            String token = UUID.randomUUID().toString();
            MimeMessage mimeMessage = emailService.constructResetTokenEmail("",
                    token, password, dto.getEmail(), dto.getFirstName(), dto.getLastName());
            mailSender.send(mimeMessage);

        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }

        NurseDTO nurseDTO = nurseService.save(dto);

        return Optional.ofNullable(nurseDTO)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Update nurse response entity.
     *
     * @param dto the dto
     * @return the response entity
     */
    @PutMapping(NURSE_MAPPING)
    public ResponseEntity<NurseDTO> updateNurse(@Valid @RequestBody NurseDTO dto) {
        if (dto.getId() == null) {
            return ResponseEntity.badRequest().build();
        }
        return Optional.ofNullable(nurseService.save(dto))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Gets nurse by id.
     *
     * @param id the id
     * @return the nurse by id
     */
    @GetMapping(NURSE_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<NurseDTO> getNurseById(@PathVariable Long id) {
        return nurseService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Delete nurse by id response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping(NURSE_MAPPING + RESOURCE_MAPPING_ID)
    public ResponseEntity<Void> deleteNurseById(@PathVariable Long id) {
        nurseService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}
