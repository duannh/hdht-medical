import { CommonModule } from "@angular/common";
import { ModuleWithProviders, NgModule } from "@angular/core";
import { AuthenticationModule } from "./authentication/authentication.module";

@NgModule({
  declarations: [],
  imports: [CommonModule, AuthenticationModule]
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [...AuthenticationModule.forRoot().providers]
    };
  }
}
