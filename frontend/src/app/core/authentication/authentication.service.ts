import { Injectable } from "@angular/core";
import { AuthService } from "ngx-auth";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap, map, switchMap, catchError } from "rxjs/operators";
import { TokenStorage } from "./token-storage.service";
import { JwtHelperService } from "@auth0/angular-jwt";
import { API } from "./../../shared/config";

const jwtHelper = new JwtHelperService();

export interface AuthResponse {
  id_token: string;
}

export interface LoginContext {
  phone: string;
  password?: string;
  rememberMe?: boolean;
}

export interface RegisterContext {
  login: string;
  email: string;
  password: string;
}
interface AccessData {
  accessToken: string;
  refreshToken: string;
}

const routes = {
  refreshToken: `${API.BASE}/refresh`,
  authenticate: `${API.BASE}/authenticate`,
  signUp: `${API.BASE}/register`,
  activate: (key: string) => `${API.BASE}/activate?key=${key}`,
  resendVerification: (email: string) =>
    `${API.BASE}/resend-email-verify?email=${email}`,
  requestReset: `${API.BASE}/account/reset-password/init`,
  resetPassword: `${API.BASE}/account/reset-password/finish`
};

@Injectable({
  providedIn: "root"
})
export class AuthenticationService implements AuthService {
  constructor(private http: HttpClient, private tokenStorage: TokenStorage) {}
  token;

  /**
   * Check, if user already authorized.
   * @description Should return Observable with true or false values
   * @returns {Observable<boolean>}
   * @memberOf AuthService
   */
  public isAuthorized(): Observable<boolean> {
    return this.tokenStorage
      .getAccessToken()
      .pipe(map(token => !jwtHelper.isTokenExpired(token)));
  }

  // Check whether the token is expired and return
  // true or false
  // return !!token; (will return either true or false based on the token availability)
  public isAuthenticated(): boolean {
    this.token = this.tokenStorage.getToken();
    return !!this.token && jwtHelper.isTokenExpired(this.token);
  }

  /**
   * Get access token
   * @description Should return access token in Observable from e.g.
   * localStorage
   * @returns {Observable<string>}
   */
  public getAccessToken(): Observable<string> {
    return this.tokenStorage.getAccessToken();
  }

  /**
   * Function, that should perform refresh token verifyTokenRequest
   * @description Should be successfully completed so interceptor
   * can execute pending requests or retry original one
   * @returns {Observable<any>}
   */
  public refreshToken(): Observable<AccessData> {
    return this.tokenStorage.getRefreshToken().pipe(
      switchMap((refreshToken: string) =>
        this.http.post(routes.refreshToken, { refreshToken })
      ),
      tap((tokens: AccessData) => this.saveAccessData(tokens)),
      catchError(err => {
        this.logout();

        return Observable.throw(err);
      })
    );
  }

  /**
   * Function, checks response of failed request to determine,
   * whether token be refreshed or not.
   * @description Essentialy checks status
   * @param {Response} response
   * @returns {boolean}
   */
  public refreshShouldHappen(response: HttpErrorResponse): boolean {
    return response.status === 401;
  }

  /**
   * Verify that outgoing request is refresh-token,
   * so interceptor won't intercept this request
   * @param {string} url
   * @returns {boolean}
   */
  public verifyTokenRequest(url: string): boolean {
    return url.endsWith("/refresh");
  }

  /**
   * EXTRA AUTH METHODS
   */

  public login(context: LoginContext): Observable<any> {
    return this.http.post<AuthResponse>(routes.authenticate, context).pipe(
      map<AuthResponse, AccessData>(res => ({
        accessToken: res.id_token,
        refreshToken: ""
      })),
      tap((tokens: AccessData) => {
        this.saveAccessData(tokens);
      })
    );
  }

  /**
   * Logout
   */
  public logout(): void {
    this.tokenStorage.clear();
  }

  /**
   * Register the user.
   * @param {RegisterContext} context The context parameters
   * @return {Observable<any>} Returns 201 if user has been created
   */
  register(context: RegisterContext): Observable<any> {
    return this.http.post(routes.signUp, context);
  }

  /**
   * resendVerificationEmail
   */
  resendVerificationEmail(email: string): Observable<any> {
    return this.http.get(routes.resendVerification(email));
  }

  /**
   * requestPasswordResetInit
   */
  requestPasswordReset(email: string): Observable<any> {
    return this.http.post(routes.requestReset, email);
  }

  /**
   * resetPasswordWithNewPassword
   */
  resetPassword(newPassword: string): Observable<any> {
    return this.http.post(routes.resetPassword, newPassword);
  }

  /**
   * activateAccount
   */
  activate(key: string) {
    return this.http.get(routes.activate(key));
  }

  /**
   * Save access data in the storage
   *
   * @private
   * @param {AccessData} data
   */
  private saveAccessData({ accessToken, refreshToken }: AccessData) {
    this.tokenStorage.setAccessToken(accessToken).setRefreshToken(refreshToken);
  }
}
