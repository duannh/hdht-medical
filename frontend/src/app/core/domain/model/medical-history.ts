import { Appointment } from "./appointment";
import { Entity } from '../interface/entity';

export interface MedicalHistory extends Entity, Appointment {
  information?: string;

  adviceFromDoctor?: string;
  
  height?: number;

  weight?: number;

  bloodGroup?: number;

  bloodPressure?: number;

  tempBody?: number;

  remedial?: string;
  
  signsOfIllness?: string;
  
  note?: string;

  appointmentId?: number;

  userId?: number;
}
