import { Entity } from "../interface/entity";
import { Authority } from "./authority";
import { MedicalHistory } from "./medical-history";

export interface User extends Entity {
  firstName?: string;

  lastName?: string;

  phone?: string;

  gender?: number;

  imageUrl?: string;

  email?: string;

  birthday?: Date;

  address?: string;

  password?: string;

  activated?: number;

  createdDate?: Date;

  lastUpdatedDate?: Date;

  authorities?: Authority[];

  authoritiesAsString?: string[];

  authoritiesAsLong?: number[];

  medicalHistories?: MedicalHistory[];
}
