import { HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";

/**
 * Extend this entity for handling common CRUD api endpoints
 */
export interface Service<D> {
  findAllWithParams(params: HttpParams): Observable<D[]>;

  findAll(): Observable<D[]>;

  findById(id: number): Observable<D>;

  save(dto: D): Observable<D>;

  update(dto: D): Observable<D>;

  deleteById(id: number): Observable<any>;
}
