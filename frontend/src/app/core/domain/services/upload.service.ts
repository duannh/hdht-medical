import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { API } from "../../../shared/config";
import { tap, catchError } from "rxjs/operators";
import {
  HandleError,
  HttpErrorHandlerService
} from "./http-error-handle.service";
import { Logger } from "app/core/logger.service";

const routes = {
  base: `${API.BASE}/upload-avatar`,
  location: `${API.BASE}/upload`
};

const log = new Logger("UploadService");

@Injectable({
  providedIn: "root"
})
export class UploadService {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = httpErrorHandler.createHandleError("UploadService");
  }

  upload(file: any): Observable<any> {
    return this.http.post<any>(routes.base, file).pipe(
    );
  }

  location(): Observable<any> {
    return this.http.get<any>(routes.location).pipe(
    );
  }

}
