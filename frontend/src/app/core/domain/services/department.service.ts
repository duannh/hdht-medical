import { Injectable } from "@angular/core";
import { API } from "app/shared/config";
import { Logger } from "app/core/logger.service";
import { Service } from "./crud-service";
import { Department } from "../model/department";
import {
  HandleError,
  HttpErrorHandlerService
} from "./http-error-handle.service";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap, catchError } from "rxjs/operators";

const routes = {
  base: `${API.BASE}/departments`
};

const log = new Logger("DepartmentService");

@Injectable({
  providedIn: "root"
})
export class DepartmentService implements Service<Department> {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = httpErrorHandler.createHandleError("CategoryService");
  }

  findAllWithParams(params: HttpParams): Observable<Department[]> {
    return this.http.get<Department[]>(routes.base, { params }).pipe(
    );
  }
  findAll(): Observable<Department[]> {
    return this.http.get<Department[]>(routes.base).pipe(
    );
  }
  findById(id: number): Observable<Department> {
    return this.http.get<Department>(routes.base + `/${id}`).pipe(
    );
  }
  save(dto: Department): Observable<Department> {
    return this.http.post<Department>(routes.base, dto).pipe(
    );
  }
  update(dto: Department): Observable<Department> {
    return this.http.put<Department>(routes.base, dto).pipe(
    );
  }
  deleteById(id: number): Observable<any> {
    return this.http.delete<Department>(`${routes.base}/${id}`).pipe();
  }
}
