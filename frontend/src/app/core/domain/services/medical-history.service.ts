import { Injectable } from "@angular/core";
import { API } from "app/shared/config";
import { Logger } from "app/core/logger.service";
import { Service } from "./crud-service";
import { MedicalHistory } from "../model/medical-history";
import {
  HandleError,
  HttpErrorHandlerService
} from "./http-error-handle.service";
import { HttpParams, HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap, catchError } from "rxjs/operators";

const routes = {
  base: `${API.BASE}/medical-histories`
};

const log = new Logger("MedicalHistoryService");

@Injectable({
  providedIn: "root"
})
export class MedicalHistoryService implements Service<MedicalHistory> {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = httpErrorHandler.createHandleError("CategoryService");
  }

  findAllWithParams(params: HttpParams): Observable<MedicalHistory[]> {
    return this.http.get<MedicalHistory[]>(routes.base, { params }).pipe(
    );
  }
  findAll(): Observable<MedicalHistory[]> {
    return this.http.get<MedicalHistory[]>(routes.base).pipe(
    );
  }
  findById(id: number): Observable<MedicalHistory> {
    return this.http.get<MedicalHistory>(routes.base + `/${id}`).pipe(
    );
  }
  save(dto: MedicalHistory): Observable<MedicalHistory> {
    return this.http.post<MedicalHistory>(routes.base, dto).pipe(
    );
  }
  update(dto: MedicalHistory): Observable<MedicalHistory> {
    return this.http.put<MedicalHistory>(routes.base, dto).pipe(
    );
  }
  deleteById(id: number): Observable<any> {
    return this.http.delete<MedicalHistory>(`${routes.base}/${id}`).pipe();
  }
}
