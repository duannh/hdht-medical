import { Injectable } from "@angular/core";
import { API } from "app/shared/config";
import { Logger } from "app/core/logger.service";
import { Service } from "./crud-service";
import {
  HandleError,
  HttpErrorHandlerService
} from "./http-error-handle.service";
import { HttpParams, HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap, catchError } from "rxjs/operators";
import { Appointment } from "../model/appointment";

const routes = {
  base: `${API.BASE}/appointments`
};

const log = new Logger("AppointmentService");

@Injectable({
  providedIn: "root"
})
export class AppointmentService implements Service<Appointment> {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = httpErrorHandler.createHandleError("CategoryService");
  }

  findAllWithParams(params: HttpParams): Observable<Appointment[]> {
    return this.http.get<Appointment[]>(routes.base, { params }).pipe(
    );
  }
  findAll(): Observable<Appointment[]> {
    return this.http.get<Appointment[]>(routes.base).pipe(
    );
  }
  findById(id: number): Observable<Appointment> {
    return this.http.get<Appointment>(routes.base + `/${id}`).pipe(
    );
  }
  save(dto: Appointment): Observable<Appointment> {
    return this.http.post<Appointment>(routes.base, dto).pipe(
    );
  }
  update(dto: Appointment): Observable<Appointment> {
    return this.http.put<Appointment>(routes.base, dto).pipe(
    );
  }
  deleteById(id: number): Observable<any> {
    return this.http.delete<Appointment>(`${routes.base}/${id}`).pipe();
  }
}
