import { Injectable } from "@angular/core";
import { API } from "app/shared/config";
import { Logger } from "app/core/logger.service";
import { Service } from "./crud-service";
import { User } from "../model/user";
import {
  HandleError,
  HttpErrorHandlerService
} from "./http-error-handle.service";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap, catchError } from "rxjs/operators";

const routes = {
  base: `${API.BASE}/users`
};

const log = new Logger("UserService");

@Injectable({
  providedIn: "root"
})
export class UserService implements Service<User> {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = httpErrorHandler.createHandleError("CategoryService");
  }

  findAllWithParams(params: HttpParams): Observable<User[]> {
    return this.http.get<User[]>(routes.base, { params }).pipe(
    );
  }
  findAll(): Observable<User[]> {
    return this.http.get<User[]>(routes.base).pipe(
    );
  }
  findById(id: number): Observable<User> {
    return this.http.get<User>(routes.base + `/${id}`).pipe(
    );
  }
  save(dto: User): Observable<User> {
    return this.http.post<User>(routes.base, dto).pipe(
    );
  }
  update(dto: User): Observable<User> {
    return this.http.put<User>(routes.base, dto).pipe(
    );
  }
  deleteById(id: number): Observable<any> {
    return this.http.delete<User>(`${routes.base}/${id}`).pipe();
  }
}
