import { Injectable } from "@angular/core";
import { API } from "app/shared/config";
import { Logger } from "app/core/logger.service";
import { Service } from "./crud-service";
import { Nurse } from "../model/nurse";
import {
  HandleError,
  HttpErrorHandlerService
} from "./http-error-handle.service";
import { Observable } from "rxjs";
import { HttpParams, HttpClient } from "@angular/common/http";
import { tap, catchError } from "rxjs/operators";

const routes = {
  base: `${API.BASE}/nurses`
};

const log = new Logger("NurseService");

@Injectable({
  providedIn: "root"
})
export class NurseService implements Service<Nurse> {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = httpErrorHandler.createHandleError("CategoryService");
  }

  findAllWithParams(params: HttpParams): Observable<Nurse[]> {
    return this.http.get<Nurse[]>(routes.base, { params }).pipe(
    );
  }
  findAll(): Observable<Nurse[]> {
    return this.http.get<Nurse[]>(routes.base).pipe(
    );
  }
  findById(id: number): Observable<Nurse> {
    return this.http.get<Nurse>(routes.base + `/${id}`).pipe(
    );
  }
  save(dto: Nurse): Observable<Nurse> {
    return this.http.post<Nurse>(routes.base, dto).pipe(
    );
  }
  update(dto: Nurse): Observable<Nurse> {
    return this.http.put<Nurse>(routes.base, dto).pipe(
    );
  }
  deleteById(id: number): Observable<any> {
    return this.http.delete<Nurse>(`${routes.base}/${id}`).pipe();
  }
}
