import { Injectable } from "@angular/core";
import { API } from "app/shared/config";
import { Logger } from "app/core/logger.service";
import { Service } from "./crud-service";
import { Doctor } from "../model/doctor";
import {
  HandleError,
  HttpErrorHandlerService
} from "./http-error-handle.service";
import { Observable } from "rxjs";
import { HttpParams, HttpClient } from "@angular/common/http";
import { tap, catchError } from "rxjs/operators";

const routes = {
  base: `${API.BASE}/doctors`
};

const log = new Logger("DoctorService");

@Injectable({
  providedIn: "root"
})
export class DoctorService implements Service<Doctor> {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = httpErrorHandler.createHandleError("CategoryService");
  }

  findAllWithParams(params: HttpParams): Observable<Doctor[]> {
    return this.http.get<Doctor[]>(routes.base, { params }).pipe(
      // tap(),
      // catchError(this.handleError("findAllByPaging", null))
    );
  }
  findAll(): Observable<Doctor[]> {
    return this.http.get<Doctor[]>(routes.base).pipe(
      // tap(),
      // catchError(this.handleError("findAll", null))
    );
  }
  findById(id: number): Observable<Doctor> {
    return this.http.get<Doctor>(routes.base + `/${id}`).pipe(
      // tap(item => log.debug("findById: " + JSON.stringify(item))),
      // catchError(this.handleError("findById", null))
    );
  }
  save(dto: Doctor): Observable<Doctor> {
    return this.http.post<Doctor>(routes.base, dto).pipe(
      // tap(item => log.debug("saveDoctor: " + JSON.stringify(item))),
      // catchError(this.handleError("saveDoctor", null))
    );
  }
  update(dto: Doctor): Observable<Doctor> {
    return this.http.put<Doctor>(routes.base, dto).pipe(
      // tap(item => log.debug("updateDoctor: " + JSON.stringify(item))),
      // catchError(this.handleError("updateDoctor", null))
    );
  }
  deleteById(id: number): Observable<any> {
    return this.http.delete<Doctor>(`${routes.base}/${id}`).pipe();
  }
}
