import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Logger } from "app/core/logger.service";
import { Observable } from "rxjs";
import { API } from "../../../shared/config";
import { User } from '../model/user';
import { Service } from "./crud-service";
import { HandleError, HttpErrorHandlerService } from "./http-error-handle.service";

const routes = {
  base: `${API.BASE}/accounts`
};

const log = new Logger("AccountService");

@Injectable({
  providedIn: "root"
})
export class AccountService implements Service<Account> {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = httpErrorHandler.createHandleError("CategoryService");
  }

  findAllWithParams(params: HttpParams): Observable<Account[]> {
    return this.http.get<Account[]>(routes.base, { params }).pipe(
    );
  }
  findAll(): Observable<Account[]> {
    return this.http.get<Account[]>(routes.base).pipe(
    );
  }
  findById(id: number): Observable<Account> {
    return this.http.get<Account>(routes.base + `/${id}`).pipe(
    );
  }
  save(dto: Account): Observable<Account> {
    return this.http.post<Account>(routes.base, dto).pipe(
    );
  }
  update(dto: Account): Observable<Account> {
    return this.http.put<Account>(routes.base, dto).pipe(
    );
  }
  deleteById(id: number): Observable<any> {
    return this.http.delete<Account>(`${routes.base}/${id}`).pipe();
  }

  getCurrentUserLogged(): Observable<User> {
    return this.http.get<User>(routes.base).pipe(
    );
  }
}
