import { Injectable } from "@angular/core";
import { API } from "app/shared/config";
import { Logger } from "app/core/logger.service";
import { Service } from "./crud-service";
import { Patient } from "../model/patient";
import {
  HandleError,
  HttpErrorHandlerService
} from "./http-error-handle.service";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap, catchError } from "rxjs/operators";

const routes = {
  base: `${API.BASE}/patients`
};

const log = new Logger("PatientService");

@Injectable({
  providedIn: "root"
})
export class PatientService implements Service<Patient> {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = httpErrorHandler.createHandleError("CategoryService");
  }

  findAllWithParams(params: HttpParams): Observable<Patient[]> {
    return this.http.get<Patient[]>(routes.base, { params }).pipe(
    );
  }
  findAll(): Observable<Patient[]> {
    return this.http.get<Patient[]>(routes.base).pipe(
    );
  }
  findById(id: number): Observable<Patient> {
    return this.http.get<Patient>(routes.base + `/${id}`).pipe(
    );
  }
  save(dto: Patient): Observable<Patient> {
    return this.http.post<Patient>(routes.base, dto).pipe(
    );
  }
  update(dto: Patient): Observable<Patient> {
    return this.http.put<Patient>(routes.base, dto).pipe(
    );
  }
  deleteById(id: number): Observable<any> {
    return this.http.delete<Patient>(`${routes.base}/${id}`).pipe();
  }
}
