import { environment } from "../../environments/environment";

const API_URL = `http://${environment.apiUrl}/api`;

export const API = {
  BASE: API_URL
};

export enum ROLE {
  // ADMIN = 1,
  // PATIENT = 4,
  // NURSE = 3,
  // DOCTOR = 2,
  // USER = 5,
  // ANONYMOUS = 6,

  ADMIN = "ROLE_ADMIN",
  PATIENT = "ROLE_PATIENT",
  NURSE = "ROLE_NURSE",
  DOCTOR = 'ROLE_DOCTOR',
  USER = 'ROLE_USER',
  ANONYMOUS = 'ANONYMOUS',
}

export enum APPOINTMENT {
  REQUEST
}