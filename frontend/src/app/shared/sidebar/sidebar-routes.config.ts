import { RouteInfo } from './sidebar.metadata';
import { ROLE } from '../config';

export const ROUTES: RouteInfo[] = [
    {
        path: '/', title: 'Bảng điều khiển', icon: 'ft-command', class: '', badge: '', badgeClass: '', submenu: [], role: [ROLE.ADMIN, ROLE.NURSE]
    },
    {
        path: 'task/list', title: 'Công việc', icon: 'ft-list', class: '', badge: '', badgeClass: '', submenu: [], role: [ROLE.DOCTOR, ROLE.NURSE, ROLE.PATIENT]
    },
    {
        path: 'medical-history/list', title: 'Lịch sử khám bệnh', icon: 'ft-file-text', class: '', badge: '', badgeClass: '', submenu: [], role: [ROLE.USER]
    },
    {
        path: '', title: 'Cuộc hẹn', icon: 'icon-notebook', class: 'has-sub', badge: '2', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', role: [ROLE.USER],
        submenu: [
            { path: 'appointments/list', title: 'Tất cả cuộc hẹn', icon: '', class: '', badge: '', badgeClass: '', submenu: [], role: [ROLE.ADMIN, ROLE.NURSE]},
            { path: 'appointments/add', title: 'Thêm cuộc hẹn', icon: '', class: '', badge: '', badgeClass: '', submenu: [], role: [ROLE.PATIENT, ROLE.NURSE, ROLE.ADMIN]},
            { path: 'appointments/waiting', title: 'Cuộc hẹn đang chờ', icon: '', class: '', badge: '', badgeClass: '', submenu: [], role: [ROLE.ADMIN, ROLE.NURSE]},
        ]
    },
    {
        path: 'doctors/list', title: 'Bác sĩ', icon: 'ft-user', class: '', badge: '', badgeClass: '', submenu: [], role: [ROLE.ADMIN, ROLE.NURSE]
    },
    {
        path: 'nurses/list', title: 'Y tá', icon: 'ft-users', class: '', badge: '', badgeClass: '', submenu: [], role: [ROLE.ADMIN]
    },
    {
        path: 'patients/list', title: 'Bệnh nhân', icon: 'ft-user-check', class: '', badge: '', badgeClass: '', submenu: [], role: [ROLE.ADMIN, ROLE.NURSE]
    },
    {
        path: 'departments/list', title: 'Khoa', icon: 'ft-user-check', class: '', badge: '', badgeClass: '', submenu: [], role: [ROLE.ADMIN]
    }
];
