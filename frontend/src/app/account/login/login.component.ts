import { Component, OnInit } from "@angular/core";
import {
  AuthenticationService,
  LoginContext
} from "./../../core/authentication/authentication.service";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  private loginContext: LoginContext = null;
  loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthenticationService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.loginForm = this.fb.group({
      phone: ["", [Validators.required]],
      password: ["", [Validators.required]],
      rememberMe: [false]
    });
  }

  onClick = () => {
    this.loginContext = {
      phone: this.loginForm.value.phone,
      password: this.loginForm.value.password
    };

    this.authService.login(this.loginContext).subscribe(e => {
      this.router.navigate(["management"]);
      this.openSnackBar("Đăng nhập thành công", this.loginForm.value.phone);
    });
  };

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }
}
