import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { PublicGuard } from 'ngx-auth';
import { RegisterComponent } from './register/register.component';
import { ForgotComponent } from './forgot/forgot.component';

const routes: Routes = [
  {
    path: "",
    children: [
      // {
      // path: "profile",
      // component: ProfileComponent
      // },
      {
        path: "login",
        component: LoginComponent,
        data: {
          title: 'Đăng nhập'
        }
        // canActivateChild: [AuthenticationGuard]
      },
      {
        path: "register",
        component: RegisterComponent,
        data: {
          title: 'Đăng ký'
        }
        // canActivateChild: [AuthenticationGuard]
      },
      {
        path: "forgot",
        component: ForgotComponent,
        data: {
          title: 'Quên mật khẩu'
        },
        // canActivateChild: [PublicGuard]
        // canActivateChild: [AuthenticationGuard]
      },
      {
        path: "",
        redirectTo: "login",
        pathMatch: "full"
      }
    ]
  },
  {
    path: "**",
    redirectTo: "login",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule {}
