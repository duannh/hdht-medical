import { Component, OnInit } from "@angular/core";
import { MatDialog, MatDialogConfig, MatSnackBar } from "@angular/material";
import { Doctor } from "app/core/domain/model/doctor";
import { DoctorService } from "app/core/domain/services/doctor.service";
import { UploadService } from "app/core/domain/services/upload.service";
import { Logger } from "app/core/logger.service";
import { DoctorsDialogFormComponent } from "../doctors-dialog-form/doctors-dialog-form.component";
import { DeleteDialogComponent } from "app/shared/delete-dialog/delete-dialog.component";

export interface UploadContext {
  urlImage?: string;
  fileName?: string;
}

const log = new Logger("DoctorsListComponent");
@Component({
  selector: "app-doctors-list",
  templateUrl: "./doctors-list.component.html",
  styleUrls: ["./doctors-list.component.scss"]
})
export class DoctorsListComponent implements OnInit {
  doctors: Doctor[];
  urlAvatar: UploadContext;
  isLoaded: boolean = false;

  constructor(
    private dialog: MatDialog,
    private doctorService: DoctorService,
    private uploadService: UploadService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.getAllDoctors();
    this.getUrlUpload();
  }

  getUrlUpload = () => {
    this.uploadService.location().subscribe(
      res => {
        this.urlAvatar = res;
      },
      err => log.error("Error", err)
    );
  };

  getAllDoctors = () => {
    this.doctorService.findAll().subscribe(res => {
      this.doctors = res;
      this.isLoaded = true;
    });
  };

  deleteDoctor(id: number) {
    // this.doctorService.deleteById(id).subscribe(
    //   data => {
    //     console.log(data);
    //     this.getAllDoctors();
    //   },
    //   error => console.log(error)
    // );
  }

  toForm(id: number) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = false;
    dialogConfig.width = "55%";
    dialogConfig.data = {
      id: id,
      title: "bác sĩ " + name
    };

    const dialogRef = this.dialog.open(
      DoctorsDialogFormComponent,
      dialogConfig
    );

    dialogRef.afterClosed().subscribe(data => {
      if (data !== undefined) {
        if (data.id) {
          this.processUpdate(data);
        } else {
          this.processSave(data);
        }
      } else {
        this.getAllDoctors();
      }
    });
  }

  processSave(model: Doctor) {
    this.doctorService.save(model).subscribe(
      res => {
        this.openSnackBar("Thêm mới thành công", "Bác sĩ");
        this.getAllDoctors();
      },
      err => {
        this.openSnackBar("Có lỗi xảy ra", "Bác sĩ");
      }
    );
  }

  processUpdate(model: Doctor) {
    this.doctorService.update(model).subscribe(
      res => {
        this.openSnackBar("Cập nhật thành công", "Bác sĩ");
        this.getAllDoctors();
      },
      err => this.openSnackBar("Có lỗi xảy ra", "Bác sĩ")
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }
}
