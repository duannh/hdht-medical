import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DoctorsListComponent } from "./doctors-list/doctors-list.component";
import { DoctorsComponent } from "./doctors.component";

const routes: Routes = [
  {
    path: "",
    component: DoctorsComponent,
    children: [
      {
        path: "list",
        component: DoctorsListComponent,
        data: {
          title: 'Danh sách bác sĩ'
        }
      }
    ]
  },
  {
    path: "**",
    redirectTo: "list",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorsRoutingModule {}
