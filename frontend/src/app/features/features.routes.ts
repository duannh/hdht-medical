import { Routes } from "@angular/router";
import { DashboardModule } from "./dashboard/dashboard.module";
import { DoctorsModule } from "./doctors/doctors.module";
import { PatientsModule } from "./patients/patients.module";
import { ProfileModule } from "./profile/profile.module";
import { DepartmentsModule } from "./departments/departments.module";
import { NursesModule } from './nurses/nurses.module';
import { AppointmentsModule } from './appointments/appointments.module';
import { MedicalHistoriesModule } from './medical-histories/medical-histories.module';
import { TasksModule } from './tasks/tasks.module';

export const FEATURES_ROUTES: Routes = [
  {
    path: "dashboard",
    loadChildren: () => DashboardModule
  },
  {
    path: "patients",
    loadChildren: () => PatientsModule
  },
  {
    path: "doctors",
    loadChildren: () => DoctorsModule
  },
  {
    path: "nurses",
    loadChildren: () => NursesModule
  },
  {
    path: "departments",
    loadChildren: () => DepartmentsModule
  },
  {
    path: "appointments",
    loadChildren: () => AppointmentsModule
  },
  {
    path: "medical-history",
    loadChildren: () => MedicalHistoriesModule
  },  
  {
    path: "task",
    loadChildren: () => TasksModule
  },
  {
    path: "profile",
    loadChildren: () => ProfileModule
  },
  {
    path: "**",
    redirectTo: "dashboard",
    pathMatch: "full"
  }
];
