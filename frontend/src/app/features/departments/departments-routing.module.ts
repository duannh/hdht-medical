import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DepartmentsComponent } from "./departments.component";
import { DepartmentsListComponent } from "./departments-list/departments-list.component";

const routes: Routes = [
  {
    path: "",
    component: DepartmentsComponent,
    children: [
      {
        path: "list",
        component: DepartmentsListComponent,
        data: {
          title: "Danh sách khoa"
        }
      }
    ]
  },
  {
    path: "**",
    redirectTo: "list",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepartmentsRoutingModule {}
