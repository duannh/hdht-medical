import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Department } from "app/core/domain/model/department";
import { DepartmentService } from "app/core/domain/services/department.service";

@Component({
  selector: "app-departments-dialog",
  templateUrl: "./departments-dialog.component.html",
  styleUrls: ["./departments-dialog.component.scss"]
})
export class DepartmentsDialogComponent implements OnInit {
  departmentForm: FormGroup;
  department: Department;
  id: number;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<DepartmentsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private departmentService: DepartmentService
  ) {
    this.id = data.id;
  }

  ngOnInit() {
    this.buildForm();
    this.getDataById();
  }

  getDataById = () => {
    const id = this.id;

    if (id != 0) {
      this.departmentService.findById(id).subscribe(res => {
        this.department = res;
        this.patchValue(this.department);
      });
    }
  };

  buildForm() {
    this.departmentForm = this.fb.group({
      id: [""],
      name: ["", [Validators.required]],
      introduction: ["", [Validators.required]],
      note: [""]
    });
  }

  save() {
    this.dialogRef.close(this.departmentForm.value);
  }

  close() {
    this.dialogRef.close();
  }

  patchValue = (model: Department) => {
    this.departmentForm.patchValue({
      id: model.id,
      name: model.name,
      note: model.note,
      introduction: model.introduction
    });
  };
}
