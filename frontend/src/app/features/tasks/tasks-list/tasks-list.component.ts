import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { Appointment } from 'app/core/domain/model/appointment';
import { Router } from '@angular/router';
import { AppointmentService } from 'app/core/domain/services/appointment.service';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.scss']
})
export class TasksListComponent implements OnInit {
  isLoaded: boolean = false;
  public array: any;
  public pageSize = 100;
  public currentPage = 0;
  public totalSize = 0;
  /**
   * New Date();
   */
  public date: Date = new Date();
  public dd: number = this.date.getDate();
  public mm: number = this.date.getMonth() + 1; //January is 0!
  public yyyy = this.date.getFullYear();
  public today: string = this.yyyy + '-' + this.mm + '-' + this.dd;
  public nextday: string = this.yyyy + '-' + this.mm + '-' + (Number(this.dd)+1);
  /**
   * set selected
   */
  public selected: string = "";
  /**
   * displayed
   */
  displayedColumns: string[] = [
    "index",
    "appointmentCode",
    "startTime",
    "finishTime",
    "location",
    "patient",
    "doctor",
    "nurse",
    "status"
  ];
  public dataSource = new MatTableDataSource<Appointment>();
  
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private appointmentService: AppointmentService,
    private snackBar: MatSnackBar,
    ) { }

  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }
  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();    
  };
  
  public suttupFilterDay() {
    this.dataSource.filterPredicate = (d, filter) => {
      return (d.startTime + '').indexOf(filter) !== -1;
    };
  };
  
  applyFilterDay() {
    this.dataSource.filter = this.selected;
  }

  ngOnInit() {
    this.getAllAppointments();    
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.array.slice(start, end);
    this.dataSource.data = part;
  }

  getAllAppointments = () => {
    this.appointmentService.findAll().subscribe(response => {
      this.isLoaded = true;
      if (response != null) {
        this.dataSource.data = response as Appointment[];
        this.array = response;
        this.totalSize = this.array.length;
        this.iterator();
      }
    });
  };

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }
}
