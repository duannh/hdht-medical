import { Component, OnInit, ViewChild } from '@angular/core';
import { MedicalHistory } from 'app/core/domain/model/medical-history';
import { MatTableDataSource, MatSort, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { MedicalHistoryService } from 'app/core/domain/services/medical-history.service';
import { MedicalHistoriesDialogComponent } from '../medical-histories-dialog/medical-histories-dialog.component';
import { DeleteDialogComponent } from 'app/shared/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-medical-histories-list',
  templateUrl: './medical-histories-list.component.html',
  styleUrls: ['./medical-histories-list.component.scss']
})
export class MedicalHistoriesListComponent implements OnInit {
  isLoaded: boolean = false;
  public array: any;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;
  displayedColumns: string[] = ["index", "appointmentId", "userId", "information", "adviceFromDoctor", "actions"];
  public dataSource = new MatTableDataSource<MedicalHistory>();

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private medicalHistoryService: MedicalHistoryService,
    private snackBar: MatSnackBar
  ) { }

  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  };


  ngOnInit() {
    this.getAllMedicalHistories();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.array.slice(start, end);
    this.dataSource.data = part;
  }

  getAllMedicalHistories = () => {
    this.medicalHistoryService.findAll().subscribe(response => {
      this.isLoaded = true;
      if (response != null) {
        this.dataSource.data = response as MedicalHistory[];
        this.array = response;
        this.totalSize = this.array.length;
        this.iterator();
      }
    });
  };

  toForm(id: number) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = false;
    dialogConfig.width = "40%";
    dialogConfig.data = {
      id: id,
      title: "lịch sử"
    };

    const dialogRef = this.dialog.open(
      MedicalHistoriesDialogComponent,
      dialogConfig
    );

    dialogRef.afterClosed().subscribe(data => {
      if (data !== undefined) {
        if (data.id) {
          this.processUpdate(data);
        } else {
          this.processSave(data);
        }
      } else {
        this.getAllMedicalHistories();
      }
    });
  }

  // openDeleteDialog(id: number, name: string) {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.disableClose = false;
  //   dialogConfig.autoFocus = false;
  //   dialogConfig.data = {
  //     id: id,
  //     title: "khoa " + name
  //   };

  //   const dialogRef = this.dialog.open(DeleteDialogComponent, dialogConfig);

  //   dialogRef.afterClosed().subscribe(data => {
  //     if (data == id) {
  //       this.medicalHistoryService.deleteById(data).subscribe(
  //         data => {
  //           this.getAllMedicalHistories();
  //           this.openSnackBar("Đã xóa thành công", "Lịch sử khám");
  //         },
  //         error => console.log(error)
  //       );
  //     }
  //   });
  // }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }

  processSave(model: MedicalHistory) {
    this.medicalHistoryService.save(model).subscribe(
      res => {
        this.openSnackBar("Thêm mới thành công", "Lịch sử khám");
        this.getAllMedicalHistories();
      },
      err => this.openSnackBar("Có lỗi xảy ra", "Lịch sử khám")
    );
  }

  processUpdate(model: MedicalHistory) {
    this.medicalHistoryService.update(model).subscribe(
      res => {
        this.openSnackBar("Cập nhật thành công", "Lịch sử khám");
        this.getAllMedicalHistories();
      },
      err => this.openSnackBar("Có lỗi xảy ra", "Lịch sử khám")
    );
  }
}
