import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "app/shared/shared.module";
import { MedicalHistoriesDialogComponent } from "./medical-histories-dialog/medical-histories-dialog.component";
import { MedicalHistoriesListComponent } from "./medical-histories-list/medical-histories-list.component";
import { MedicalHistoriesRoutingModule } from "./medical-histories-routing.module";
import { MedicalHistoriesComponent } from "./medical-histories.component";

@NgModule({
  declarations: [
    MedicalHistoriesListComponent,
    MedicalHistoriesComponent,
    MedicalHistoriesDialogComponent
  ],
  imports: [CommonModule, MedicalHistoriesRoutingModule, SharedModule],
  entryComponents: [MedicalHistoriesDialogComponent]
})
export class MedicalHistoriesModule {}
