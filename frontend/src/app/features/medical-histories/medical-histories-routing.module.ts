import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MedicalHistoriesComponent } from './medical-histories.component';
import { MedicalHistoriesListComponent } from './medical-histories-list/medical-histories-list.component';

const routes: Routes = [
  {
    path: "",
    component: MedicalHistoriesComponent,
    children: [
      {
        path: "list",
        component: MedicalHistoriesListComponent,
        data: {
          title: "Danh sách lịch sử"
        }
      }
    ]
  },
  {
    path: "**",
    redirectTo: "list",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MedicalHistoriesRoutingModule {}
