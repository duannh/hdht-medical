import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MedicalHistory } from 'app/core/domain/model/medical-history';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MedicalHistoryService } from 'app/core/domain/services/medical-history.service';
import { Appointment } from 'app/core/domain/model/appointment';
import { AppointmentService } from 'app/core/domain/services/appointment.service';
import { UserService } from 'app/core/domain/services/user.service';
import { User } from 'app/core/domain/model/user';
import { Patient } from 'app/core/domain/model/patient';
import { PatientService } from 'app/core/domain/services/patient.service';

@Component({
  selector: 'app-medical-histories-dialog',
  templateUrl: './medical-histories-dialog.component.html',
  styleUrls: ['./medical-histories-dialog.component.scss']
})
export class MedicalHistoriesDialogComponent implements OnInit {
  medicalHistoryForm: FormGroup;
  medicalHistory: MedicalHistory;
  public appointments: Appointment[];
  public patients: Patient[];
  id: number;
  
  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<MedicalHistoriesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private medicalHistoryService: MedicalHistoryService,
    private appointmentService: AppointmentService,
    private patientService: PatientService
  ) { 
    this.id = data.id;
  }

  ngOnInit() {
    this.buildForm();
    this.getDataById();
    this.getAppointment();
    this.getPatient();
  }

  getAppointment() {
    this.appointmentService.findAll().subscribe(res => {
      this.appointments = res;
      console.log(this.appointments);
      
    });
  }

  getPatient() {
    this.patientService.findAll().subscribe(res => {
      this.patients = res;
      console.log(this.patients);
      
    });
  }

  getDataById = () => {
    const id = this.id;

    if (id != 0) {
      this.medicalHistoryService.findById(id).subscribe(res => {
        this.medicalHistory = res;
        this.patchValue(this.medicalHistory);
      });
    }
  };

  buildForm() {
    this.medicalHistoryForm = this.fb.group({
      id: [""],
      information: ["",[Validators.required]],
      adviceFromDoctor: [""],
      height: ["",[Validators.required]],
      weight: ["",[Validators.required]],
      bloodGroup: ["",[Validators.required]],
      bloodPressure: ["",[Validators.required]],
      tempBody: ["",[Validators.required]],
      remedial: [""],
      signsOfIllness: [""],
      note: [""],
      appointmentId: [0, Validators.minLength(1)],
      userId: [0, Validators.minLength(1)],
    });
  }

  save() {
    this.dialogRef.close(this.medicalHistoryForm.value);
  }

  close() {
    this.dialogRef.close();
  }

  patchValue = (model: MedicalHistory) => {
    this.medicalHistoryForm.patchValue({
      id: model.id,
      information: model.information,
      adviceFromDoctor: model.adviceFromDoctor,
      height: model.height,
      weight: model.weight,
      bloodGroup: model.bloodGroup,
      bloodPressure: model.bloodPressure,
      tempBody: model.tempBody,
      remedial: model.remedial,
      signsOfIllness: model.signsOfIllness,
      note: model.note,
      appointmentId: model.appointmentId,
      userId: model.userId,
    });
  };
}
