import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialog, MatDialogConfig, MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from "@angular/material";
import { Department } from "app/core/domain/model/department";
import { Nurse } from "app/core/domain/model/nurse";
import { DepartmentService } from "app/core/domain/services/department.service";
import { NurseService } from "app/core/domain/services/nurse.service";
import { DeleteDialogComponent } from "app/shared/delete-dialog/delete-dialog.component";

interface Gender {
  id: number;
  value: string;
}

@Component({
  selector: "app-nurses-dialog-form",
  templateUrl: "./nurses-dialog-form.component.html",
  styleUrls: ["./nurses-dialog-form.component.scss"]
})
export class NursesDialogFormComponent implements OnInit {
  public nurseForm: FormGroup;
  public nurse: Nurse;
  public id: number;
  public departments: Department[];
  public genders: Gender[] = [
    { id: 0, value: "Chọn giới tính" },
    { id: 1, value: "Nam" },
    { id: 2, value: "Nữ" },
    { id: 3, value: "Khác" }
  ];

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<NursesDialogFormComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private nursesService: NurseService,
    private departmentService: DepartmentService,
    private snackBar: MatSnackBar
  ) {
    this.id = data.id;
  }

  ngOnInit() {
    this.buildForm();
    this.getDepartments();
    this.getDataById();
  }

  getDepartments() {
    this.departmentService.findAll().subscribe(res => {
      this.departments = res;
    });
  }

  getDataById = () => {
    const id = this.id;

    if (id != 0) {
      this.nursesService.findById(id).subscribe(res => {
        this.nurse = res;
        this.patchValue(this.nurse);
      });
    }
  };

  buildForm() {
    this.nurseForm = this.fb.group({
      firstName: ["", [Validators.required]],
      lastName: ["", [Validators.required]],
      function: ["", [Validators.required]],
      email: [""],
      phone: [
        {
          value: "",
          disabled: this.id ? true : false
        },
        [
          Validators.required,
          Validators.minLength(7),
          Validators.maxLength(12)
        ]
      ],
      id: [""],
      departmentId: [0, Validators.minLength(1)],
      gender: [this.genders[0].id],
      address: [""],
      userId: [""]
    });
  }

  save() {
    this.dialogRef.close(this.nurseForm.value);
  }

  close() {
    this.dialogRef.close();
  }

  patchValue = (model: Nurse) => {
    this.nurseForm.patchValue({
      firstName: model.firstName,
      id: model.id,
      userId: model.userId,
      lastName: model.lastName,
      departmentId: model.departmentId,
      function: model.function,

      // user
      phone: model.phone.substring(1, model.phone.length),
      gender: model.gender,
      email: model.email,
      address: model.address
    });
  };

  openDeleteDialog(id: number) {
    const dialogConfig = new MatDialogConfig();
    const name =
      this.nurseForm.value.firstName + " " + this.nurseForm.value.lastName;

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      id: id,
      title: "y tá " + name
    };

    const dialogRef = this.dialog.open(DeleteDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(data => {
      if (data == id) {
        this.nursesService.deleteById(data).subscribe(
          data => {
            this.openSnackBar("Đã xóa thành công", "Y tá");
            this.close();
          },
          error => this.openSnackBar("Có lỗi xảy ra", "Y tá")
        );
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }
}
