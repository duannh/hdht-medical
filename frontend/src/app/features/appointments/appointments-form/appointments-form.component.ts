import { AfterViewInit, Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialog, MatSnackBar } from "@angular/material";
import { ActivatedRoute, Router } from '@angular/router';
import { Appointment } from "app/core/domain/model/appointment";
import { Department } from "app/core/domain/model/department";
import { Doctor } from "app/core/domain/model/doctor";
import { Patient } from "app/core/domain/model/patient";
import { AppointmentService } from "app/core/domain/services/appointment.service";
import { DepartmentService } from "app/core/domain/services/department.service";
import { DoctorService } from "app/core/domain/services/doctor.service";
import { PatientService } from "app/core/domain/services/patient.service";
import { Observable } from "rxjs";
import { map, startWith } from "rxjs/operators";
// import * as $ from 'jquery';

@Component({
  selector: "app-appointments-form",
  templateUrl: "./appointments-form.component.html",
  styleUrls: ["./appointments-form.component.scss"]
})
export class AppointmentsFormComponent
  implements OnInit, AfterViewInit, OnDestroy {
  private patientControl = new FormControl();
  private doctorControl = new FormControl();
  private form: FormGroup;
  private appointment: Appointment;
  private doctors: Doctor[];
  private doctor: Doctor;
  private patients: Patient[];
  private departments: Department[];
  private patient: Patient;
  private id: number;
  private patientSelected: boolean = false;
  // subscription: SubscriptionLike;
  private filteredPatients: Observable<Patient[]>;
  // private filteredDoctors: Observable<Doctor[]>;

  constructor(
    private fb: FormBuilder,
    private appointmentService: AppointmentService,
    private doctorService: DoctorService,
    private patientService: PatientService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private departmentService: DepartmentService,
    private router: Router
  ) {}

  ngOnInit() {
    this.buildForm();
    this.getDataById();
    // this.getAllDoctors();
    this.getAllPatients();
    this.getDepartments();
  }

  ngOnDestroy() {
    // unsubscribe
    // if (this.subscription) {
    // this.subscription.unsubscribe();
    // }
  }

  ngAfterViewInit() {}

  private getAllPatients = () => {
    this.patientService.findAll().subscribe((response: Patient[]) => {
      if (response != null) {
        this.patients = response;

        this.filteredPatients = this.form.controls.patientName.valueChanges.pipe(
          startWith(""),
          map(value => this._filterPeople(this.patients, value))
        );
      }
    });
  };

  // private getAllDoctors = () => {
  //   this.doctorService.findAll().subscribe((response: Doctor[]) => {
  //     if (response != null) {
  //       this.doctors = response;

  //       this.filteredPatients = this.form.controls.doctorName.valueChanges.pipe(
  //         startWith(""),
  //         map(value => this._filterPeople(this.doctors, value))
  //       );
  //     }
  //   });
  // };

  private getDepartments() {
    this.departmentService.findAll().subscribe(res => {
      this.departments = res;
    });
  }

  private getDataById = () => {
    if(this.route.snapshot.url[1]){
      this.id = Number.parseInt(this.route.snapshot.url[1].path);
    }
    if (undefined != this.id && this.id != 0) {
      this.appointmentService.findById(this.id).subscribe(res => {
        this.appointment = res;
        this.patchValue(this.appointment);
      });
    }
  };

  private buildForm() {
    this.form = this.fb.group({
      startTime: ["", [Validators.required]],
      startDate: new FormControl(new Date(new Date().setHours(0, 0, 0, 0))),
      startMinus: [""],
      departmentId: [0, Validators.required],
      appointmentCode: [""],
      createdBy: [""],
      patientId: ["", [Validators.required]],
      patientName: [""],
      status: [""],
      note: [""],
      id: [""],
      reason: [""],
      // patient
      cardId: [
        {
          value: "",
          disabled: true
        }
      ],
      insuranceCardNumber: [
        {
          value: "",
          disabled: true
        }
      ],
      addressPatient: [
        {
          value: "",
          disabled: true
        }
      ],
      phonePatient: [
        {
          value: "",
          disabled: true
        }
      ],
      finishTime: [
        {
          value: "",
          disabled: true
        }
      ]
    });
  }

  private save() {
    // (this.form.value);
  }

  private findPatientById = (id: number) => {
    this.patientService.findById(id).subscribe(res => {
      this.patient = res;
      this.patientSelected = true;

      this.form.controls["patientId"].setValue(this.patient.id);
      this.form.controls["cardId"].setValue(this.patient.cardId);

      this.form.controls["insuranceCardNumber"].setValue(
        this.patient.insuranceCardNumber
      );

      this.form.controls["addressPatient"].setValue(this.patient.address);
      this.form.controls["phonePatient"].setValue(this.patient.phone);
    });
  };

  // private findDoctorById = (id: number) => {
  //   this.doctorService.findById(id).subscribe(res => {
  //     this.doctor = res;
  //     this.doctorSelected = true;
  //   });
  // };

  close() {}

  patchValue = (model: Appointment) => {
    this.findPatientById(model.patientId);
    this.patientService.findById(model.patientId).subscribe(res => {
      this.patient = res;
      this.patientSelected = true;
      this.form.patchValue({
        id: model.id,
        startTime: this.millisToUTCDate(model.startTime),
        startMinus: this.millisToUTCDate(model.startTime),
        departmentId: model.departmentId,
        appointmentCode: model.appointmentCode,
        createdBy: model.createdBy,
        finishTime: this.millisToUTCDate(model.finishTime),
        patientId: model.patientId,
        patientName: this.patient.firstName + " " + this.patient.lastName,
        status: model.status,
        note: model.note,
        reason: model.reason
      });
    })

  };

  // openDeleteDialog(id: number) {
  //   const dialogConfig = new MatDialogConfig();

  //   dialogConfig.disableClose = false;
  //   dialogConfig.autoFocus = false;
  //   dialogConfig.data = {
  //     id: id,
  //     title: "cuộc hẹn " + id
  //   };

  //   const dialogRef = this.dialog.open(DeleteDialogComponent, dialogConfig);

  //   dialogRef.afterClosed().subscribe(data => {
  //     if (data == id) {
  //       this.appointmentService.deleteById(data).subscribe(
  //         data => {
  //           this.openSnackBar("Đã xóa thành công", "Cuộc hẹn");
  //         },
  //         error => this.openSnackBar("Có lỗi xảy ra", "Cuộc hẹn")
  //       );
  //     }
  //   });
  // }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }

  private _filterPeople(arr: any[], value: string): any[] {
    const filterValue = value;
    const peopleGroup = arr;
    this.patientSelected = false;

    if (typeof value == "number" || typeof value == "object") {
      this.form.controls["patientName"].setValue("");
      return peopleGroup;
    }

    let listAfterFilter = peopleGroup.filter(
      option =>
        option.firstName
          .toLocaleLowerCase()
          .indexOf(filterValue.trim().toLocaleLowerCase()) !== -1
    );

    if (listAfterFilter.length == 0) {
      listAfterFilter = peopleGroup.filter(
        option =>
          option.lastName
            .toLocaleLowerCase()
            .indexOf(filterValue.trim().toLocaleLowerCase()) !== -1
      );
    }

    return listAfterFilter;
  }

  private seletedPatient(optionValue: Patient) {
    this.findPatientById(optionValue.id);
    this.form.controls["patientName"].setValue(
      optionValue.firstName + " " + optionValue.lastName
    );
  }

  private changePatient(item: any) {
    if (item.currentTarget.value == "") {
      this.patientSelected = false;
      return;
    }
  }

  private addMinutes(date, minutes) {
    return new Date(date.getTime() + minutes * 60000);
  }

  private onChangeMinus(minus) {
    // new DatePipe("en-US").transform(minus, "yyyy-MM-dd");
    this.form.controls["startTime"].setValue(minus);
    this.form.controls["finishTime"].setValue(this.addMinutes(minus, 30));
  }

  onSubmit(){
    if(this.id){
      let appointment: any = this.appointment || {};
    
      const startTime = new Date(this.form.controls.startDate.value);
      startTime.setDate(new Date(this.form.controls.startDate.value).getDate());
      startTime.setHours(new Date(this.form.controls.startMinus.value).getHours());
      startTime.setMinutes(new Date(this.form.controls.startMinus.value).getMinutes());

      const timeOfService = new Date(Date.UTC(startTime.getFullYear(), startTime.getMonth(), startTime.getDate(), startTime.getHours(), startTime.getMinutes()));
      
      const finishTime = new Date(this.form.controls.startDate.value);
      finishTime.setDate(new Date(this.form.controls.startDate.value).getDate());
      finishTime.setHours(new Date(this.form.controls.finishTime.value).getHours());
      finishTime.setMinutes(new Date(this.form.controls.finishTime.value).getMinutes());

      const timeFinishOfService = new Date(Date.UTC(finishTime.getFullYear(), finishTime.getMonth(), finishTime.getDate(), finishTime.getHours(), finishTime.getMinutes()));

      appointment.startTime = (timeOfService);
      appointment.finishTime = (timeFinishOfService);
      appointment.note = this.form.controls.note.value;
      appointment.reason = this.form.controls.reason.value;
      appointment.patientId = this.form.controls.patientId.value;
      appointment.departmentId = this.form.controls.departmentId.value;

      this.appointmentService.update(appointment).subscribe(res => {
        this.openSnackBar("Cập nhật thành công cuộc hẹn " + res.appointmentCode, "Cuộc hẹn");
        this.router.navigate(["management/appointments/list"]);
      }, err => {
        this.openSnackBar("Có lỗi xảy ra, vui lòng thử lại", "Cuộc hẹn");
      })

    } else {
      let appointment: any = this.appointment || {};
    
      const startTime = new Date(this.form.controls.startDate.value);
      startTime.setDate(new Date(this.form.controls.startDate.value).getDate());
      startTime.setHours(new Date(this.form.controls.startMinus.value).getHours());
      startTime.setMinutes(new Date(this.form.controls.startMinus.value).getMinutes());

      const timeOfService = new Date(Date.UTC(startTime.getFullYear(), startTime.getMonth(), startTime.getDate(), startTime.getHours(), startTime.getMinutes()));
      
      const finishTime = new Date(this.form.controls.startDate.value);
      finishTime.setDate(new Date(this.form.controls.startDate.value).getDate());
      finishTime.setHours(new Date(this.form.controls.finishTime.value).getHours());
      finishTime.setMinutes(new Date(this.form.controls.finishTime.value).getMinutes());

      const timeFinishOfService = new Date(Date.UTC(finishTime.getFullYear(), finishTime.getMonth(), finishTime.getDate(), finishTime.getHours(), finishTime.getMinutes()));

      appointment.startTime = (timeOfService);
      appointment.finishTime = (timeFinishOfService);
      appointment.note = this.form.controls.note.value;
      appointment.reason = this.form.controls.reason.value;
      appointment.patientId = this.form.controls.patientId.value;
      appointment.departmentId = this.form.controls.departmentId.value;

      this.appointmentService.save(appointment).subscribe(res => {
        this.openSnackBar("Đã tạo thành công cuộc hẹn, vui lòng kiểm tra lại thông tin", "Cuộc hẹn");
        this.router.navigate(["management/appointments/info/", res.id]);
      }, err => {
        this.openSnackBar("Có lỗi xảy ra, vui lòng thử lại", "Cuộc hẹn");
      })
    }
  }
  
  changeTimezone(date) {

    // suppose the date is 12:00 UTC
    var invdate = new Date(date.toLocaleString('en-US', {
      timeZone: 'Asia/Ho_Chi_Minh'
    }));
  
    // then invdate will be 07:00 in Toronto
    // and the diff is 5 hours
    var diff = date.getTime() - invdate.getTime();
  
    // so 12:00 in Toronto is 17:00 UTC
    return new Date(date.getTime() + diff);
  
  }

  toUTCDate = function(date){
    var _utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    return _utc;
  };

  millisToUTCDate = function(millis){
    return this.toUTCDate(new Date(millis));
  };

  onReset(){
    this.patient = null;
    this.patientSelected = false;
    this.form.patchValue({
      startTime: new Date(new Date().setHours(0, 0, 0, 0)),
      startMinus: '',
      departmentId: '',
      finishTime: '',
      patientId: '',
      patientName: '',
      note: '',
      reason: ''
    });
  }
}
