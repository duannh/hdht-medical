import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "app/shared/shared.module";
import { AppointmentsFilterComponent } from "./appointments-filter/appointments-filter.component";
import { AppointmentsFormComponent } from "./appointments-form/appointments-form.component";
import { AppointmentsListComponent } from "./appointments-list/appointments-list.component";
import { AppointmentsRoutingModule } from "./appointments-routing.module";
import { AppointmentsComponent } from "./appointments.component";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { AppointmentsWaitComponent } from './appointments-wait/appointments-wait.component';
import { AppointmentsAssignComponent } from './appointments-assign/appointments-assign.component';
import { AppointmentsInfoComponent } from './appointments-info/appointments-info.component';

@NgModule({
  declarations: [
    AppointmentsComponent,
    AppointmentsFormComponent,
    AppointmentsListComponent,
    AppointmentsFilterComponent,
    AppointmentsWaitComponent,
    AppointmentsAssignComponent,
    AppointmentsInfoComponent
  ],
  imports: [
    CommonModule,
    AppointmentsRoutingModule,
    SharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ]
})
export class AppointmentsModule {}
