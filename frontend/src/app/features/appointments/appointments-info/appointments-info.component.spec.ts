import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentsInfoComponent } from './appointments-info.component';

describe('AppointmentsInfoComponent', () => {
  let component: AppointmentsInfoComponent;
  let fixture: ComponentFixture<AppointmentsInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentsInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentsInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
