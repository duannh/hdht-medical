import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatDialog,
  MatDialogConfig,
  MatSnackBar,
  MatSort,
  MatTableDataSource
} from "@angular/material";
import { Appointment } from "app/core/domain/model/appointment";
import { AppointmentService } from "app/core/domain/services/appointment.service";
import { DeleteDialogComponent } from "app/shared/delete-dialog/delete-dialog.component";
import { Router } from '@angular/router';

@Component({
  selector: "app-appointments-list",
  templateUrl: "./appointments-list.component.html",
  styleUrls: ["./appointments-list.component.scss"]
})
export class AppointmentsListComponent implements OnInit {
  isLoaded: boolean = false;
  public array: any;
  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;
  displayedColumns: string[] = [
    "index",
    "appointmentCode",
    "date",
    "startTime",
    "finishTime",
    "patient",
    "doctor",
    "departmentName",
    "status",
    "actions"
  ];
  public dataSource = new MatTableDataSource<Appointment>();
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private appointmentService: AppointmentService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  };

  ngOnInit() {
    this.getAllAppointments();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.array.slice(start, end);
    this.dataSource.data = part;
  }

  getAllAppointments = () => {
    this.appointmentService.findAll().subscribe(response => {
      this.isLoaded = true;
      if (response != null) {
        this.dataSource.data = response as Appointment[];
        this.array = response;
        this.totalSize = this.array.length;
        this.iterator();
      }
    });
  };

  toForm(id: number) {
    if(id === 0){
      this.router.navigate(["management/appointments/add"]);
    } else {
      this.router.navigate(["management/appointments/edit/", id]);
    }
  }

  openDeleteDialog(id: number, code: string) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      id: id,
      title: "cuộc hẹn " + code
    };

    const dialogRef = this.dialog.open(DeleteDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(data => {
      if (data == id) {
        this.appointmentService.deleteById(data).subscribe(
          data => {
            this.openSnackBar("Đã xóa thành công", "Cuộc hẹn");
            this.getAllAppointments();
          },
          error => this.openSnackBar("Có lỗi xảy ra", "Cuộc hẹn")
        );
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }

  processSave(model: Appointment) {
    this.appointmentService.save(model).subscribe(
      res => {
        this.openSnackBar("Thêm mới thành công", "Khoa");
        this.getAllAppointments();
      },
      err => this.openSnackBar("Có lỗi xảy ra", "Khoa")
    );
  }

  processUpdate(model: Appointment) {
    this.appointmentService.update(model).subscribe(
      res => {
        this.openSnackBar("Cập nhật thành công", "Khoa");
        this.getAllAppointments();
      },
      err => this.openSnackBar("Có lỗi xảy ra", "Khoa")
    );
  }

  toUTCDate = function(date){
    var _utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    return _utc;
  };

  millisToUTCDate = function(millis){
    return this.toUTCDate(new Date(millis));
  };

  changeTimezone(date) {

    // suppose the date is 12:00 UTC
    var invdate = new Date(date.toLocaleString('en-US', {
      timeZone: 'Asia/Ho_Chi_Minh'
    }));
  
    // then invdate will be 07:00 in Toronto
    // and the diff is 5 hours
    var diff = date.getTime() - invdate.getTime();
  
    // so 12:00 in Toronto is 17:00 UTC
    return new Date(date.getTime() + diff);
  
  }
}
