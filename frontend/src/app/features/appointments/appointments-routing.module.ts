import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppointmentsFormComponent } from "./appointments-form/appointments-form.component";
import { AppointmentsListComponent } from "./appointments-list/appointments-list.component";
import { AppointmentsComponent } from "./appointments.component";
import { AppointmentsInfoComponent } from './appointments-info/appointments-info.component';
import { AppointmentsAssignComponent } from './appointments-assign/appointments-assign.component';

const routes: Routes = [
  {
    path: "",
    component: AppointmentsComponent,
    data: {
      title: "Cuộc hẹn"
    },
    children: [
      {
        path: "list",
        component: AppointmentsListComponent,
        data: {
          title: "Danh sách cuộc hẹn"
        }
      },
      {
        path: "add",
        component: AppointmentsFormComponent,
        data: {
          title: "Tạo cuộc hẹn"
        }
      },
      {
        path: "assign",
        component: AppointmentsAssignComponent,
        data: {
          title: "Tạo cuộc hẹn"
        }
      },
      {
        path: "waiting",
        component: AppointmentsAssignComponent,
        data: {
          title: "Tạo cuộc hẹn"
        }
      },
      {
        path: "info/:id",
        component: AppointmentsInfoComponent,
        data: {
          title: "Thông tin cuộc hẹn"
        }
      },
      {
        path: "edit/:id",
        component: AppointmentsFormComponent,
        data: {
          title: "Chỉnh sửa cuộc hẹn"
        }
      }
    ]
  },
  {
    path: "**",
    redirectTo: "dashboard",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppointmentsRoutingModule {}
