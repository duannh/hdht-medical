import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentsAssignComponent } from './appointments-assign.component';

describe('AppointmentsAssignComponent', () => {
  let component: AppointmentsAssignComponent;
  let fixture: ComponentFixture<AppointmentsAssignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentsAssignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentsAssignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
