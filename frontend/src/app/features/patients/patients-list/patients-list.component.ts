import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatDialog,
  MatDialogConfig,
  MatSnackBar,
  MatSort,
  MatTableDataSource
} from "@angular/material";
import { Patient } from "app/core/domain/model/patient";
import { PatientService } from "app/core/domain/services/patient.service";
import { DeleteDialogComponent } from "app/shared/delete-dialog/delete-dialog.component";
import { PatientsDialogFormComponent } from "../patients-dialog-form/patients-dialog-form.component";

@Component({
  selector: "app-patients-list",
  templateUrl: "./patients-list.component.html",
  styleUrls: ["./patients-list.component.scss"]
})
export class PatientsListComponent implements OnInit {
  private isLoaded: boolean = false;
  private array: any;
  private pageSize = 5;
  private currentPage = 0;
  private totalSize = 0;
  private displayedColumns: string[] = [
    "index",
    "name",
    "phone",
    "cardId",
    "gender",
    "actions"
  ];
  private dataSource = new MatTableDataSource<Patient>();

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private patientService: PatientService,
    private snackBar: MatSnackBar
  ) {}

  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  };

  ngOnInit() {
    this.getAllPatients();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.array.slice(start, end);
    this.dataSource.data = part;
  }

  private getAllPatients = () => {
    this.patientService.findAll().subscribe(response => {
      this.isLoaded = true;
      if (response != null) {
        this.dataSource.data = response as Patient[];
        this.array = response;
        this.totalSize = this.array.length;
        this.iterator();
      }
    });
  };

  private toForm(id: number) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = false;
    dialogConfig.width = "40%";
    dialogConfig.data = {
      id: id,
      title: "bệnh nhân " + name
    };

    const dialogRef = this.dialog.open(
      PatientsDialogFormComponent,
      dialogConfig
    );

    dialogRef.afterClosed().subscribe(data => {
      if (data !== undefined) {
        if (data.id) {
          this.processUpdate(data);
        } else {
          this.processSave(data);
        }
      } else {
        this.getAllPatients();
      }
    });
  }

  private openDeleteDialog(id: number, name: string) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      id: id,
      title: "bệnh nhân " + name
    };

    const dialogRef = this.dialog.open(DeleteDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(data => {
      if (data == id) {
        this.patientService.deleteById(data).subscribe(
          data => {
            this.getAllPatients();
            this.openSnackBar("Đã xóa thành công", "Bệnh nhân");
          },
          error => console.log(error)
        );
      }
    });
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }

  private processSave(model: Patient) {
    this.patientService.save(model).subscribe(
      res => {
        this.openSnackBar("Thêm mới thành công", "Bệnh nhân");
        this.getAllPatients();
      },
      err => this.openSnackBar("Có lỗi xảy ra", "Bệnh nhân")
    );
  }

  private processUpdate(model: Patient) {
    this.patientService.update(model).subscribe(
      res => {
        this.openSnackBar("Cập nhật thành công", "Bệnh nhân");
        this.getAllPatients();
      },
      err => this.openSnackBar("Có lỗi xảy ra", "Bệnh nhân")
    );
  }
}
