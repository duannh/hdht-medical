import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialogConfig,
  MatDialog,
  MatSnackBar
} from "@angular/material";
import { Patient } from "app/core/domain/model/patient";
import { PatientService } from "app/core/domain/services/patient.service";
import { Gender } from "app/features/doctors/doctors-dialog-form/doctors-dialog-form.component";
import { DeleteDialogComponent } from "app/shared/delete-dialog/delete-dialog.component";

@Component({
  selector: "app-patients-dialog-form",
  templateUrl: "./patients-dialog-form.component.html",
  styleUrls: ["./patients-dialog-form.component.scss"]
})
export class PatientsDialogFormComponent implements OnInit {
  private patientForm: FormGroup;
  private patient: Patient;
  private id: number;
  private genders: Gender[] = [
    { id: 0, value: "Chọn giới tính" },
    { id: 1, value: "Nam" },
    { id: 2, value: "Nữ" },
    { id: 3, value: "Khác" }
  ];

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<PatientsDialogFormComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private patientService: PatientService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {
    this.id = data.id;
  }

  ngOnInit() {
    this.buildForm();
    this.getDataById();
  }

  getDataById = () => {
    const id = this.id;

    if (id != 0) {
      this.patientService.findById(id).subscribe(res => {
        this.patient = res;
        this.patchValue(this.patient);
      });
    }
  };

  buildForm() {
    this.patientForm = this.fb.group({
      firstName: ["", [Validators.required]],
      lastName: ["", [Validators.required]],
      cardId: ["", [Validators.required]],
      insuranceCardNumber: [""],
      email: [""],
      phone: [
        {
          value: "",
          disabled: this.id ? true : false
        },
        [
          Validators.required,
          Validators.minLength(7),
          Validators.maxLength(12)
        ]
      ],
      id: [""],
      gender: [this.genders[0].id],
      userId: [""]
    });
  }

  save() {
    this.dialogRef.close(this.patientForm.value);
  }

  close() {
    this.dialogRef.close();
  }

  patchValue = (model: Patient) => {
    this.patientForm.patchValue({
      firstName: model.firstName,
      id: model.id,
      userId: model.userId,
      lastName: model.lastName,

      // user
      phone: model.phone.substring(1, model.phone.length),
      gender: model.gender,
      email: model.email,
      cardId: model.cardId,
      insuranceCardNumber: model.insuranceCardNumber
    });
  };

  openDeleteDialog(id: number) {
    const dialogConfig = new MatDialogConfig();
    const name =
      this.patientForm.value.firstName + " " + this.patientForm.value.lastName;

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      id: id,
      title: "bệnh nhân " + name
    };

    const dialogRef = this.dialog.open(DeleteDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(data => {
      if (data == id) {
        this.patientService.deleteById(data).subscribe(
          data => {
            this.openSnackBar("Đã xóa thành công", "Bệnh nhân");
            this.close();
          },
          error => this.openSnackBar("Có lỗi xảy ra", "Bệnh nhân")
        );
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }
}
