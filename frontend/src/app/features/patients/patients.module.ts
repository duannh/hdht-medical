import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "app/shared/shared.module";
import { PatientsDialogFormComponent } from "./patients-dialog-form/patients-dialog-form.component";
import { PatientsListComponent } from "./patients-list/patients-list.component";
import { PatientsRoutingModule } from "./patients-routing.module";
import { PatientsComponent } from "./patients.component";

@NgModule({
  declarations: [
    PatientsComponent,
    PatientsListComponent,
    PatientsDialogFormComponent
  ],
  imports: [CommonModule, PatientsRoutingModule, SharedModule],
  entryComponents: [PatientsDialogFormComponent]
})
export class PatientsModule {}
