import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PatientsListComponent } from "./patients-list/patients-list.component";
import { PatientsComponent } from "./patients.component";

const routes: Routes = [
  {
    path: "",
    component: PatientsComponent,
    children: [
      {
        path: "list",
        component: PatientsListComponent,
        data: {
          title: "Danh sách bệnh nhân"
        }
      }
    ]
  },
  {
    path: "**",
    redirectTo: "list",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientsRoutingModule {}
