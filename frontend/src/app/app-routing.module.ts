import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ProtectedGuard, PublicGuard } from 'ngx-auth';
import { AccountModule } from "./account/account.module";
import { FeaturesComponent } from "./features/features.component";
import { FEATURES_ROUTES } from "./features/features.routes";

const appRoutes: Routes = [
  {
    path: "management",
    component: FeaturesComponent,
    children: FEATURES_ROUTES,
    canActivateChild: [ProtectedGuard]
  },
  {
    path: "auth",
    loadChildren: () => AccountModule,
    canActivateChild: [PublicGuard]
  },
  {
    path: "",
    redirectTo: "management",
    pathMatch: "full"
  },
  {
    path: "**",
    redirectTo: "management",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
